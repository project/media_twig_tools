<?php

namespace Drupal\media_twig_tools\Generators;

use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\media_twig_tools\Services\ImageBuilderInterface;
use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;
use Drupal\media_twig_tools\Services\Traits\SettingsTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Generate images based on options.
 */
class ImageGenerator {

  use SettingsTrait;

  /**
   * The Image Builder service.
   *
   * @var \Drupal\media_twig_tools\Services\ImageBuilderInterface
   */
  protected ImageBuilderInterface $imageBuilder;

  /**
   * File.
   *
   * @var \Drupal\file\Entity\File
   */
  protected File $file;

  /**
   * Effects.
   *
   * @var array
   */
  protected array $effects;

  /**
   * Style.
   *
   * @var \Drupal\image\ImageStyleInterface|null
   */
  protected ?ImageStyleInterface $style;

  /**
   * Dirname.
   *
   * @var string
   */
  protected string $dirname;

  /**
   * Filename.
   *
   * @var string
   */
  protected string $filename;

  /**
   * Extension.
   *
   * @var string
   */
  protected string $extension;

  /**
   * Destination.
   *
   * @var string|null
   */
  protected ?string $destination;

  /**
   * Space.
   *
   * @var string
   */
  protected string $space;

  /**
   * Constructor.
   *
   * @param array $pathData
   *   The path data.
   * @param \Drupal\media_twig_tools\Services\ImageBuilderInterface $imageBuilder
   *   The image builder.
   */
  public function __construct(array $pathData, ImageBuilderInterface $imageBuilder) {
    $this->imageBuilder = $imageBuilder;

    $this->file = File::load($pathData['f']);
    $this->effects = $this->getEffects($pathData);
    $this->style = $pathData[MediaTwigToolsInterface::OPTION_STYLE] === $this->imageBuilder->getNoStyleName() ? NULL : ImageStyle::load($pathData[MediaTwigToolsInterface::OPTION_STYLE]);
    $this->style = $this->style ?? ImageStyle::create([]);
    $this->dirname = $pathData['dirname'];
    $this->filename = $pathData['filename'];
    $this->extension = $pathData['extension'];

    $uri = explode('://', $this->file->getFileUri());
    $this->space = $uri ? reset($uri) : 'public';
  }

  /**
   * Return the space.
   *
   * @return string
   *   The space.
   */
  public function getSpace(): string {
    return $this->space;
  }

  /**
   * Generate the image.
   *
   * @return bool
   *   True if file exists;
   */
  public function generate() {
    if ($this->fileExists()) {
      return TRUE;
    }

    // Check source.
    $this->checkSourceFile();

    // Init weight.
    $effects = $this->style->getEffects()->getConfiguration();
    $weight = count($effects) > 0 ? end($effects)['weight'] : 0;

    // Add image style effects.
    foreach ($this->effects as $effect) {
      $this->style->addImageEffect($effect + ['weight' => $weight++]);
    }

    // Generate the image.
    $this->style->createDerivative($this->file->getFileUri(), $this->getDestination());

    return $this->fileExists();
  }

  /**
   * Return the destination of the generated file.
   *
   * @return string
   *   The destination.
   */
  public function getDestination(): string {
    if (!isset($this->destination)) {
      $path = (str_starts_with($this->dirname, '/') ? substr($this->dirname, 1) : $this->dirname) . '/' . $this->filename . '.' . $this->extension;
      $this->destination = urldecode($this->imageBuilder->getFileRealPathFromUri($path, $this->space));
    }
    return $this->destination;
  }

  /**
   * Check if generated file already exists.
   *
   * @return bool
   *   File exists.
   */
  public function fileExists(): bool {
    return file_exists($this->getDestination());
  }

  /**
   * Check source file.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function checkSourceFile(): void {
    if (!file_exists($this->file->getFileUri())) {
      // Try to build source file.
      $this->imageBuilder->buildOriginalImage($this->file);
      if (!file_exists($this->file->getFileUri())) {
        throw new NotFoundHttpException();
      }
    }
  }

  /**
   * Return mime type.
   *
   * @return string
   *   The mime type.
   */
  public function getMimeType(): string {
    return 'image/' . $this->extension;
  }

  /**
   * Return file size.
   *
   * @return string
   *   The file size.
   */
  public function getFileSize(): string {
    if ($this->fileExists()) {
      return filesize($this->getDestination());
    }
    return '';
  }

}
