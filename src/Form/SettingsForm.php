<?php

namespace Drupal\media_twig_tools\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\image\ImageStyleStorageInterface;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfig;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface;
use Drupal\media_twig_tools\Services\GeneratedFilesManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Media Twig Tools form.
 */
class SettingsForm extends FormBase {

  /**
   * The list of existing images style ids.
   *
   * @var int[]|string[]
   */
  protected array $existingImageStylesIds;

  /**
   * The image style storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected ImageStyleStorageInterface $imageStyleStorage;

  /**
   * Constructor.
   *
   * @param \Drupal\media_twig_tools\Config\MediaTwigToolsConfig $conf
   *   The media twig tools config.
   * @param \Drupal\media_twig_tools\Services\GeneratedFilesManagerInterface $generatedFilesManager
   *   The generated files manager interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    protected MediaTwigToolsConfig $conf,
    protected GeneratedFilesManagerInterface $generatedFilesManager,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->imageStyleStorage = $entity_type_manager->getStorage('image_style');
    $this->existingImageStylesIds = array_keys($this->imageStyleStorage->loadMultiple());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(MediaTwigToolsConfigInterface::SERVICE_ID),
      $container->get(GeneratedFilesManagerInterface::SERVICE_ID),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_twig_tools_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Settings.
    $form['settings'] = $this->getSettingsFormPart($form_state, empty($form['clearer']));

    // Clear.
    $form['clearer'] = $this->getClearerFormPart($form_state);

    // Format and effects.
    $form['format'] = $this->getFormatFormPart($form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (isset($triggering_element['#submit_action']) && $submitAction = $triggering_element['#submit_action']) {
      switch ($submitAction) {
        case 'clear':
          $this->clearImages($form_state);
          break;

        case 'save':
          $this->saveSettings($form_state);
          break;
      }
    }
  }

  /**
   * Init settings.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param bool $open
   *   The part should be open by default.
   *
   * @return array
   *   The settings form part.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  protected function getSettingsFormPart(FormStateInterface $form_state, $open = FALSE) {
    $settings = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#tree' => TRUE,
      '#open' => $open,
    ];

    $settings[MediaTwigToolsConfigInterface::FIELD_DIRNAME] = [
      '#type' => 'textfield',
      '#title' => $this->t('Generated image directory name'),
      '#default_value' => $this->conf->getDirname(),
      '#description' => $this->t('The name of the directory of the generated images'),
      '#required' => TRUE,
    ];

    $settings[MediaTwigToolsConfigInterface::FIELD_NO_STYLE] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this, 'isAnExistingImageStyleName'],
        'replace_pattern' => '[^a-z0-9_]+',
      ],
      '#title' => $this->t('No image style identifier'),
      '#description' => $this->t('The effect identifier of a no styled image base'),
      '#default_value' => $this->conf->getNoStyle(),
      '#required' => TRUE,
    ];

    $settings[MediaTwigToolsConfigInterface::FIELD_ALLOW_MEMORY_LIMIT] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allow memory limit (for image generation)'),
      '#default_value' => $this->conf->getAllowMemoryLimit(),
      '#description' => 'MB',
    ];

    $settings[MediaTwigToolsConfigInterface::FIELD_ENABLE_FORMAT_CHECK] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable format check'),
      '#default_value' => $this->conf->isFormatCheckEnable(),
      '#description' => $this->t('If enabled, only printed urls will be able to generate new Media Twig Tools image styles.'),
    ];

    $settings[MediaTwigToolsConfigInterface::FIELD_ENABLE_OVERSIZE_CHECK] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable oversize check'),
      '#default_value' => $this->conf->isOversizeCheckEnable(),
      '#description' => $this->t('If enabled, effects that possibly could generate an image larger than possible will not be printed.'),
    ];

    $settings[MediaTwigToolsConfigInterface::FIELD_ALLOW_OLD_EFFECTS_FORMAT] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable old effects format'),
      '#default_value' => $this->conf->isOldEffectsFormatEnable(),
      '#description' => $this->t('If enabled, will generate images called with the old existing effects url format. Although, it will ot create new files with the old format.'),
    ];

    $settings['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save settings'),
        '#submit_action' => 'save',
      ],
    ];

    return $settings;
  }

  /**
   * Init the clear images form part.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form part.
   */
  protected function getClearerFormPart(FormStateInterface $form_state) {
    $clearer = [
      '#type' => 'details',
      '#title' => $this->t('Generated images'),
      '#open' => FALSE,
      '#tree' => TRUE,
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Delete generated images'),
          '#submit_action' => 'clear',
        ],
      ],
    ];

    // List of builder dirs.
    foreach ($this->generatedFilesManager->getDirs() as $space => $subdirectories) {
      $space_base = $this->generatedFilesManager->getSpaceBaseDir($space);
      $options = array_combine($subdirectories, $subdirectories);
      $options = array_map(function ($path) use ($space_base) {
        $basename = basename($path);
        $imageStyle = $this->imageStyleStorage->load($basename);
        return $this->t(
          '@label (@detail)',
          [
            '@label' => $imageStyle ? $imageStyle->label() : 'No image style',
            '@detail' => str_replace($space_base, '', $path),
          ]);
      }, $options);

      if (!empty($subdirectories)) {
        $clearer['types'][$space] = [
          '#type' => 'checkboxes',
          '#title' => $space,
          '#options' => $options,
        ];
      }
    }

    if (empty($clearer['types'])) {
      $clearer[] = [
        '#markup' => $this->t('No built images found.'),
      ];
      unset($clearer['actions']);
    }

    return $clearer;
  }

  /**
   * Return true if the $value is an existing image style id.
   */
  public function isAnExistingImageStyleName($value) {
    return isset($this->existingImageStylesIds[$value]);
  }

  /**
   * Save settings action.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function saveSettings(FormStateInterface $form_state) {
    $settings = $form_state->getValue('settings');
    $this->conf
      ->setDirname($settings[MediaTwigToolsConfigInterface::FIELD_DIRNAME])
      ->setNoStyle($settings[MediaTwigToolsConfigInterface::FIELD_NO_STYLE])
      ->setAllowMemoryLimit($settings[MediaTwigToolsConfigInterface::FIELD_ALLOW_MEMORY_LIMIT])
      ->enableFormatCheck($settings[MediaTwigToolsConfigInterface::FIELD_ENABLE_FORMAT_CHECK])
      ->enableOversizeCheck($settings[MediaTwigToolsConfigInterface::FIELD_ENABLE_OVERSIZE_CHECK])
      ->enableOldEffectsFormat($settings[MediaTwigToolsConfigInterface::FIELD_ALLOW_OLD_EFFECTS_FORMAT]);
  }

  /**
   * Clear images action.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function clearImages(FormStateInterface $form_state) {
    foreach ($form_state->getValues()['clearer']['types'] as $type) {
      foreach (array_filter($type) as $dirname) {
        $this->generatedFilesManager->cleanDir($dirname);
      }
    }
  }

  /**
   * Build format and effects form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The format form.
   */
  protected function getFormatFormPart(FormStateInterface $form_state) {
    $form = [
      '#type' => 'details',
      '#title' => $this->t('Generated effects'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    // Views links.
    $link = Link::createFromRoute($this->t('Effects list'), 'view.media_twig_tools_effects.page_1');
    $link = $link->toRenderable();
    $link['#attributes'] = ['class' => ['button', 'button-action', 'button--primary']];
    $form[] = $link;

    return $form;
  }

}
