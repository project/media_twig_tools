<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileStorageInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface;
use Drupal\media_twig_tools\Generators\ImageGenerator;
use Drupal\media_twig_tools\Services\Traits\ReplaceTrait;
use Drupal\media_twig_tools\Services\Traits\SettingsTrait;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * This class builds image from settings.
 */
class ImageBuilder implements ImageBuilderInterface {

  use ReplaceTrait;
  use SettingsTrait;

  /**
   * Effects dir cache.
   *
   * @var array
   */
  protected array $effectsDirCache = [];

  /**
   * Path info.
   *
   * @var string
   */
  protected string $pathInfo;

  /**
   * The file storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected FileStorageInterface $fileStorage;

  /**
   * Image style storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $imageStyleStorage;

  /**
   * Cache id.
   *
   * @var string
   */
  protected string $cacheId;

  /**
   * Constructor.
   *
   * @param \Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface $mediaTwigToolsConfig
   *   The config.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator.
   * @param \GuzzleHttp\Client $httpClient
   *   The http client.
   * @param \Drupal\media_twig_tools\Services\EffectsManagerInterface $effectsManager
   *   The effects manager.
   * @param \Drupal\media_twig_tools\Services\GeneratedFilesManagerInterface $builtFilesManager
   *   The built files manager.
   * @param \Drupal\media_twig_tools\Services\OversizeCheckerInterface $oversizeChecker
   *   The oversize checker.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   */
  public function __construct(
    protected MediaTwigToolsConfigInterface $mediaTwigToolsConfig,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
    protected Client $httpClient,
    protected EffectsManagerInterface $effectsManager,
    protected GeneratedFilesManagerInterface $builtFilesManager,
    protected OversizeCheckerInterface $oversizeChecker,
    RequestStack $request_stack,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->pathInfo = $request_stack->getCurrentRequest()->getPathInfo();
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->imageStyleStorage = $entity_type_manager->getStorage('image_style');
  }

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   *
   * @deprecated in media_twig_tools:2.1.0 and is removed from media_twig_tools:2.2.0
   *   Use \Drupal:service(MediaTwigToolsInterface::SERVICE_NAME);
   *
   * @see https://www.drupal.org/project/media_twig_tools/issues/3369533
   */
  public static function me() {
    return \Drupal::service(ImageBuilderInterface::SERVICE_ID);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheId(): string {
    if (!isset($this->cacheId)) {
      $this->cacheId = '?' . http_build_query(['t' => (new Random())->name()]);
    }
    return $this->cacheId;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUriFromSettings(string $file_id, array $settings, array $options): ?string {
    $imgUri = NULL;

    // Get file.
    $file = $this->fileStorage->load($file_id);

    if ($file) {
      $final_style = $this->generateStyle($settings, $options);

      $uri = $this->getStyledUriFromFileId(
        $file->id(),
        $final_style,
      );

      // Check image oversize.
      if ($this->oversizeChecker->isOversizedProhibited($options, $settings)
        && $this->oversizeChecker->isGeneratedFileOversized($file, $settings)) {
        return NULL;
      }

      $imgUri = $this->getImageUrlFromFileAndSettings($file->id(), $uri, $settings, $options);
      // @todo check relative url need;
    }

    return $imgUri;
  }

  /**
   * Return the effects from the dirname.
   *
   * @param string $path
   *   THe path.
   *
   * @return array
   *   The effects.
   */

  /**
   * {@inheritdoc}
   */
  public function getEffectsFromPathString($path) {
    $effects = $this->effectsManager->getEffectsByToken($path);

    // Compatibility.
    if ($this->mediaTwigToolsConfig->isOldEffectsFormatEnable() && !isset($effects)) {
      $effects = $this->getOldEffectsFormatFromPath($path);
    }

    // Filter effects.
    return $effects;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsWithStyle(array $srcSettings) {
    $style = $this->getStyle($srcSettings);
    if ($style) {
      $srcSettings[MediaTwigToolsInterface::OPTION_STYLE] = $style;
    }
    else {
      unset($srcSettings[MediaTwigToolsInterface::OPTION_STYLE]);
    }

    return $srcSettings;
  }

  /**
   * Return the dirname according to effects data.
   *
   * @param array $effects_data
   *   The effects data.
   *
   * @return string
   *   The effects dirname.
   */
  public function getEffectsDirnameFromEffectsData(array $effects_data = []) {
    $id = $this->effectsManager->serialize($effects_data);
    if (!isset($this->effectsDirCache[$id])) {
      // Check new effects token format (v3).
      $effect_token = $this->effectsManager->getEffectToken($effects_data);

      if (!$effect_token) {
        if ($this->mediaTwigToolsConfig->isOldEffectsFormatEnable()) {
          $effect_token = $this->getOldEffectsDir($effects_data);
        }

        // New effect token.
        if (!$effect_token) {
          $effect_token = $this->effectsManager->createEffectToken($effects_data);
        }
      }

      $this->effectsDirCache[$id] = $effect_token;
    }

    return $this->effectsDirCache[$id];
  }

  /**
   * Return true if $path is an image builder route.
   *
   * @param string $path
   *   THe path.
   *
   * @return bool
   *   The status of the image.
   */
  public function isImageBuilderRoute(string $path) {
    $pathInfo = pathinfo($path);
    $pathData = explode('/', $pathInfo['dirname']);
    $mediaDirnameIndex = count($pathData) - 3;

    return isset($pathData[$mediaDirnameIndex]) && $pathData[$mediaDirnameIndex] === $this->getMediaDirname();
  }

  /**
   * Return the build image dirname.
   *
   * @return string
   *   THe media dirname.
   */
  public function getMediaDirname() {
    return $this->mediaTwigToolsConfig->getDirname();
  }

  /**
   * Return the no style name.
   *
   * @return string
   *   THe no style name.
   */
  public function getNoStyleName() {
    return $this->mediaTwigToolsConfig->getNoStyle();
  }

  /**
   * Generate the image and return the generator.
   *
   * @param string $path
   *   The image path.
   *
   * @return \Drupal\media_twig_tools\Generators\ImageGenerator
   *   The Image Generator.
   *
   * @throws \Exception
   */
  public function generateImage(string $path) {
    if ($allow_memory_limit = $this->mediaTwigToolsConfig->getAllowMemoryLimit()) {
      ini_set('memory_limit', $allow_memory_limit . 'M');
    }

    // Check generation rights.
    $path_data = $this->getDataFromPath($path);
    if ($this->mediaTwigToolsConfig->isFormatCheckEnable()) {
      $this->checkAskedFormat($path_data);
    }

    $generator = new ImageGenerator($path_data, $this);
    $generator->generate();

    return $generator;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAskedFormat(array $path_data) {
    // Build theoretical  path from path settings.
    $options = $path_data + [
      MediaTwigToolsInterface::OPTION_RELATIVE => TRUE,
    ];

    $settings = $path_data + [
      MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE => TRUE,
    ];

    $uri = $this->getImageUriFromSettings($path_data['f'], $settings, $options);
    $canonical_path = $this->cleanPath($this->getUrl($uri));
    $current_path = $this->cleanPath($this->pathInfo);

    if ($current_path != $canonical_path) {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Return image builder data from image path.
   *
   * @param string $path
   *   The path.
   *
   * @return array
   *   The data.
   *
   * @throws \Exception
   */
  public function getDataFromPath($path) {
    if (!$this->isImageBuilderRoute($path)) {
      throw new \Exception('Not a media builder path');
    }

    $pathInfo = pathinfo($path);

    // File id.
    $nameData = explode('_', $pathInfo['basename']);
    $pathInfo['f'] = reset($nameData);
    $pathInfo['original_basename'] = implode('_', array_slice($nameData, 1));

    // Settings and options.
    $settings = explode('/', $pathInfo['dirname']);
    $count = count($settings);
    $pathInfo[MediaTwigToolsInterface::OPTION_EFFECTS] = $this->getEffectsFromPathString(end($settings));
    $pathInfo[MediaTwigToolsInterface::OPTION_STYLE] = $settings[$count - 2];

    return $pathInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUrlFromFileAndSettings(string $file_id, string $uri, array $settings, array $options): ?string {
    // Path data.
    $url_path = pathinfo($uri);
    $dir_path = pathinfo($url_path['dirname']);
    $dir_space = explode('://', $dir_path['dirname'])[0];

    // Get style.
    $style_settings = $this->getSettingsWithStyle($settings);
    $style = $this->getStyle($style_settings, $settings, $options);

    // Get Effects.
    $effects = $this->getEffects($style_settings);

    return $this->replace(
      '@dir/@mediaDir/@style/@effects/@fid_@file.@extension',
      [
        '@dir' => $dir_space . '://styles',
        '@mediaDir' => $this->getMediaDirname(),
        '@style' => $style ? $style->id() : $this->getNoStyleName(),
        '@effects' => $this->getEffectsDirnameFromEffectsData($effects),
        '@file' => $url_path['filename'],
        '@extension' => $url_path['extension'],
        '@fid' => $file_id,
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getScaledAndCroppedUri(string $file_id, int $width, int $height, array $options = []) {
    $file = $this->fileStorage->load(intval($file_id));

    if ($file) {
      $effects = [
        [
          "id" => "image_scale_and_crop",
          "data" => [
            "width" => $width,
            "height" => $height,
          ],
        ],
      ];

      return $this->getUriWithEffectsFromMedia($file, $effects, $options);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getUriWithEffectsFromMedia(FileInterface $file, array $effects, array $options = []) {
    $srcSettings = [
      MediaTwigToolsInterface::OPTION_EFFECTS => $effects,
    ];

    return $this->getImageUriFromSettings($file->id(), $srcSettings, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function buildOriginalImage(?FileInterface $file = NULL): void {
    if ($file) {
      $this->buildImageFile($file->getFileUri());
    }
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function buildImageFile(string $uri): void {
    $url = $this->fileUrlGenerator->generateAbsoluteString($uri);
    try {
      $this->httpClient->get($url);
    }
    catch (\Error $e) {
      // Mute error...
    }
    catch (\Exception $e) {
      // Mute exception...
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStyledUriFromFileId($file_id, ?ImageStyleInterface $style = NULL): ?string {
    $uri = NULL;
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->fileStorage->load(intval($file_id));
    if ($file) {
      if ($style) {
        $uri = $style->buildUri($file->getFileUri());
      }
      else {
        $uri = $file->getFileUri();
      }
    }

    return $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileRealPathFromUri(string $uri, string $space = 'public'): string {
    $path = str_replace(
      substr($this->fileUrlGenerator->generateString($space . '://-'), 1, -1),
      '/',
      $uri
    );

    return $space . '://' . $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getStyle(array|string|null $array_1 = [], array $array_2 = [], array $array_3 = []): ?ImageStyleInterface {
    // Let the availability to build a style from the name.
    $style = $array_1;
    if (is_array($style)) {
      $styles = array_filter(
        [
          $style[MediaTwigToolsInterface::OPTION_STYLE] ?? NULL,
          $array_2[MediaTwigToolsInterface::OPTION_STYLE] ?? NULL,
          $array_3[MediaTwigToolsInterface::OPTION_STYLE] ?? NULL,
        ]);
      $style = reset($styles);
    }

    if ($style instanceof ImageStyleInterface) {
      return $style;
    }
    if (is_string($style)) {
      return $this->imageStyleStorage->load($style);
    }
    return NULL;
  }

  /**
   * Generate an url from the uri.
   *
   * @param string $uri
   *   The input uri.
   * @param array|null $options
   *   The options.
   * @param bool|null $absolute
   *   Force absolute if true.
   *
   * @return string
   *   The url.
   */
  public function getUrl(string $uri, ?array $options = [], ?bool $absolute = NULL): string {
    $relative = TRUE;
    if (isset($absolute)) {
      $relative = !$absolute;
    }
    elseif (isset($options[MediaTwigToolsInterface::OPTION_RELATIVE])) {
      $relative = $options[MediaTwigToolsInterface::OPTION_RELATIVE];
    }

    $url = $relative ? $this->fileUrlGenerator->generateString($uri) : $this->fileUrlGenerator->generateAbsoluteString($uri);
    return $url . $this->getCacheId();
  }

  /**
   * Generate a style combining all effects.
   *
   * @param array $settings
   *   The settings.
   * @param array $options
   *   The options.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   The style.
   */
  protected function generateStyle(array $settings, array $options) {
    // Get base style from options.
    /** @var \Drupal\image\ImageStyleInterface $style */
    $style = $this->getStyle($settings, $options) ?? $this->imageStyleStorage->create(['id' => $this->mediaTwigToolsConfig->getNoStyle()]);

    $effects = $this->getEffects($settings);
    foreach ($effects as $effect) {
      $style->addImageEffect($effect);
    }

    return $style;
  }

  /**
   * Get the effects from the old format (compatibility).
   *
   * @param string $path
   *   The path.
   *
   * @return array
   *   The effects.
   */
  protected function getOldEffectsFormatFromPath(string $path): array {
    $effects = [];
    foreach (explode(static::EFFECTS_SEPARATOR, $path) as $data) {
      // Split letters and numbers.
      $split = preg_split("/(,?\s+)|((?<=[a-z])(?=\d))|((?<=\d)(?=[a-z]))/i", $data);

      // Associative array.
      if (is_numeric(reset($split))) {
        $key = 1;
        $delta = -1;
      }
      else {
        $key = 0;
        $delta = 1;
      }
      $effect = [
        'id' => array_pop($split),
        'data' => [],
      ];
      for ($i = $key; $i <= count($split) - $delta; $i += 2) {
        if (isset($split[$i + $delta]) && isset($split[$i])) {
          $effect['data'][$split[$i]] = $split[$i + $delta];
        }
      }

      $effects[] = $effect;
    }
    return $effects;
  }

  /**
   * Return the old effect if directory exists (Compatibility).
   *
   * @param array $effects_data
   *   The effects.
   *
   * @return string|null
   *   The old effect dir if exists.
   */
  protected function getOldEffectsDir(array $effects_data): ?string {
    $effects = [];
    foreach ($effects_data as $weight => $effect) {
      $effects[$weight] = '';
      foreach ($effect['data'] as $key => $value) {
        $effects[$weight] .= $key . $value;
      }
      $effects[$weight] .= $effect['id'];
    }

    // Check if a directory already exists.
    $dir = implode(static::EFFECTS_SEPARATOR, $effects);

    // Flatten.
    $dirs = [];
    array_map(function ($item) use (&$dirs) {
      $dirs = array_merge($item, $dirs);
    }, $this->builtFilesManager->getDirs());

    // Filter existing dirs.
    $existing_dirs = array_filter($dirs, function ($item) use ($dir) {
      return is_dir($item . '/' . $dir);
    });

    if (!empty($existing_dirs)) {
      return $dir;
    }
    return NULL;
  }

  /**
   * Return a clean path.
   *
   * @param string $path
   *   The path.
   *
   * @return string
   *   The clean path.
   */
  protected function cleanPath(string $path): string {
    return $this->replace(parse_url($path)['path'], ['//' => '/']);
  }

}
