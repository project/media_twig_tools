<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Component\Plugin\PluginBase;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\image\ImageStyleInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for media_twig_tools_formatter plugins.
 */
abstract class MediaTwigToolsFormatterPluginBase extends PluginBase implements MediaTwigToolsFormatterInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Image type prefix.
   *
   * @const string
   */
  public const PREFIX_IMAGE = 'image';

  /**
   * Media Plugin manager.
   *
   * @var \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface
   */
  protected MediaTwigToolsFormatterPluginManagerInterface $formatterManager;

  /**
   * Media Twig tools.
   *
   * @var \Drupal\media_twig_tools\Services\MediaTwigToolsInterface
   */
  protected MediaTwigToolsInterface $mediaTwigTools;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Oversize checker.
   *
   * @var \Drupal\media_twig_tools\Services\OversizeCheckerInterface
   */
  protected OversizeCheckerInterface $oversizeChecker;

  /**
   * Image builder.
   *
   * @var \Drupal\media_twig_tools\Services\ImageBuilderInterface
   */
  protected ImageBuilderInterface $imageBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The image builder.
   *
   * @var \Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface
   */
  protected MediaTwigToolsConfigInterface $conf;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Effects Manager.
   *
   * @var \Drupal\media_twig_tools\Services\EffectsManagerInterface
   */
  protected EffectsManagerInterface $effectsManager;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * Valid options.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   *
   * @return bool
   *   The valid status.
   */
  abstract protected function isValid(MediaInterface $media, array $options): bool;

  /**
   * Return the item markup.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The markup.
   */
  abstract protected function getMarkup(MediaInterface $media, array $options): array;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance
      ->setFileSystem($container->get('file_system'))
      ->setLanguageManager($container->get('language_manager'))
      ->setEntityTypeManager($container->get('entity_type.manager'))
      ->setMessenger($container->get('messenger'))
      ->setFormatterManager($container->get(MediaTwigToolsFormatterPluginManagerInterface::SERVICE_ID))
      ->setMediaTwigTools($container->get(MediaTwigToolsInterface::SERVICE_ID))
      ->setOversizeChecker($container->get(OversizeCheckerInterface::SERVICE_ID))
      ->setImageBuilder($container->get(ImageBuilderInterface::SERVICE_ID))
      ->setConf($container->get(MediaTwigToolsConfigInterface::SERVICE_ID))
      ->setEffectsManager($container->get(EffectsManagerInterface::SERVICE_ID));

    return $instance;
  }

  /**
   * Set the formatter manager.
   *
   * @param \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface $formatter_manager
   *   The formatter manager.
   *
   * @return self
   *   Self.
   */
  protected function setFormatterManager(MediaTwigToolsFormatterPluginManagerInterface $formatter_manager): self {
    $this->formatterManager = $formatter_manager;
    return $this;
  }

  /**
   * Set the media twig tools service.
   *
   * @param \Drupal\media_twig_tools\Services\MediaTwigToolsInterface $media_twig_tools
   *   The media twig tools service.
   *
   * @return self
   *   Self.
   */
  protected function setMediaTwigTools(MediaTwigToolsInterface $media_twig_tools): self {
    $this->mediaTwigTools = $media_twig_tools;
    return $this;
  }

  /**
   * Set entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return $this
   */
  protected function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): self {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Set messenger.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   *
   * @return $this
   */
  protected function setMessenger(MessengerInterface $messenger): self {
    $this->messenger = $messenger;
    return $this;
  }

  /**
   * Set oversize checker.
   *
   * @param \Drupal\media_twig_tools\Services\OversizeCheckerInterface $oversize_checker
   *   The oversize checker.
   *
   * @return $this
   */
  protected function setOversizeChecker(OversizeCheckerInterface $oversize_checker): self {
    $this->oversizeChecker = $oversize_checker;
    return $this;
  }

  /**
   * Set image builder.
   *
   * @param \Drupal\media_twig_tools\Services\ImageBuilderInterface $image_builder
   *   The image builder.
   *
   * @return $this
   */
  protected function setImageBuilder(ImageBuilderInterface $image_builder): self {
    $this->imageBuilder = $image_builder;
    return $this;
  }

  /**
   * Set the language manager.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   *
   * @return $this
   */
  protected function setLanguageManager(LanguageManagerInterface $language_manager): self {
    $this->languageManager = $language_manager;
    return $this;
  }

  /**
   * Set the conf.
   *
   * @param \Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface $conf
   *   The conf.
   *
   * @return $this
   */
  protected function setConf(MediaTwigToolsConfigInterface $conf): self {
    $this->conf = $conf;
    return $this;
  }

  /**
   * Set the file system.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   *
   * @return $this
   */
  protected function setFileSystem(FileSystemInterface $file_system): self {
    $this->fileSystem = $file_system;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  final public function build(MediaInterface $media, array $options): array {
    if ($this->isValid($media, $options)) {
      return $this->getMarkup($media, $options);
    }
    return [];
  }

  /**
   * Set the effect manager.
   *
   * @param \Drupal\media_twig_tools\Services\EffectsManagerInterface $effects_manager
   *   The effect manager.
   *
   * @return self
   *   Self.
   */
  public function setEffectsManager(EffectsManagerInterface $effects_manager): MediaTwigToolsFormatterPluginBase {
    $this->effectsManager = $effects_manager;
    return $this;
  }

  /**
   * Log validation error.
   *
   * @param string $message
   *   The message.
   */
  public function addValidationError(string $message): void {
    $this->messenger->addError(
      $this->t('@plugin could not process. @message',
               [
                 '@plugin' => $this->pluginId,
                 '@message' => $message,
               ]
      ));
  }

  /**
   * Initialize the image class.
   *
   * @param array $formatter_build
   *   The formatter build array.
   * @param array $options
   *   The options.
   */
  protected function initImagesClasses(array &$formatter_build, array $options) {
    if (!empty($options[MediaTwigToolsInterface::OPTION_CLASS])) {
      $class = is_array($options[MediaTwigToolsInterface::OPTION_CLASS]) ? $options[MediaTwigToolsInterface::OPTION_CLASS] : explode(
        ' ',
        $options[MediaTwigToolsInterface::OPTION_CLASS]
      );

      if (array_key_exists('#attributes', $formatter_build) && $formatter_build['#attributes']['class']) {
        $formatter_build['#attributes']['class'] = array_merge($formatter_build['#attributes']['class'], $class);
      }
      else {
        $formatter_build['#attributes']['class'] = $class;
      }
    }
  }

  /**
   * Return the source build array.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The main media.
   * @param string $media_query
   *   The media query.
   * @param string|ImageStyleInterface|null $type
   *   The Image style defining the type attribute of the source tag.
   * @param array $settings
   *   The settings.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The source tag.
   */
  protected function buildSource(MediaInterface $media, string $media_query = 'default', $type = 'default', array $settings = [], array $options = []): array {
    /*
     * If type and media query are the default ones, it means we try to build
     * the default source. In this case, we need to render an img tag,
     * not a source.
     */
    if ($type === 'default' && $media_query === 'default') {
      return [];
    }

    // Get src data.
    $srcset = $this->getSrcSetValues($media, $settings, $options);

    if (empty($srcset)) {
      return [];
    }

    return [
      '#theme' => 'source',
      '#settings' => $settings,
      '#options' => $options,
      '#attributes' => array_filter(
        [
          'srcset' => $this->getFormattedSrcSetAttributes($srcset),
          'type' => $this->getTypeExtension($type),
          'media' => $media_query === 'default' ? NULL : $media_query,
        ]),
    ];
  }

  /**
   * Return the srcset value.
   *
   * Build an srcset value according to settings.
   * Each setting should generate a specific url according to a media query.
   * If any setting is not valid (for example due to over sizing effects), the
   * url will not be generated, and will not be added to the srcset.
   * If the srcset is null, it shows that no fallback url is valid. In this case
   * we suppose there should not be any fallback url available in the DOM, cause
   * the main image url in the src should be the only one matching to the
   * wanted image.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $settings
   *   The settings.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The src set if at least one url is valid.
   */
  protected function getSrcSetValues(MediaInterface $media, array $settings = [], array $options = []): array {
    // Get the media past in top params or media query settings.
    $current_media = $this->mediaTwigTools->getMediaEntityFromTwigParam($settings['media'] ?? $media);
    $media = reset($current_media);

    // Get srcset value.
    return $this->getSrcSetFromSourceSettings($media, $settings, $options);
  }

  /**
   * Return the type extension.
   *
   * @param string|ImageStyleInterface $type
   *   The type.
   *
   * @return string|null
   *   The type extension.
   */
  protected function getTypeExtension($type): ?string {
    if ($type instanceof ImageStyleInterface) {
      return $this->imageBuilder->replace(
        '@prefix/@extension',
        [
          '@prefix' => static::PREFIX_IMAGE,
          '@extension' => $type->getDerivativeExtension(''),
        ]
      );
    }
    return NULL;
  }

  /**
   * Return src set from source settings.
   *
   * @param \Drupal\media\Entity\Media $media
   *   The media.
   * @param array $settings
   *   The src settings.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The list of src attributes.
   */
  protected function getSrcSetFromSourceSettings(Media $media, array $settings, array $options): array {
    // Build src urls.
    $srcset = [];

    if (isset($settings['srcset'])) {
      $srcset = $this->getSrcList($media, $settings, $options);
    }
    elseif (isset($settings[MediaTwigToolsInterface::OPTION_EFFECTS])) {
      $file_id = $this->getFileId($media, $settings, $options);

      $uri = $this->getImageUri($file_id, $settings, $options);
      $srcset = [
        [
          'uri' => $uri,
          'url' => $this->imageBuilder->getUrl($uri),
        ],
      ];
    }

    return $srcset;
  }

  /**
   * Return the url image from settings.
   *
   * @param string $file_id
   *   The file id.
   * @param string|array $settings
   *   The settings.
   * @param array $options
   *   The options.
   *
   * @return array|string|string[]|null
   *   The image path.
   */
  public function getImageUri(string $file_id, $settings, array $options) {
    // Build srcset.
    if ($settings === 'default') {
      return $this->imageBuilder->getStyledUriFromFileId(
        $file_id,
        $this->imageBuilder->getStyle($options[MediaTwigToolsInterface::OPTION_STYLE]),
      );
    }
    if (is_array($settings)) {
      return $this->imageBuilder->getImageUriFromSettings($file_id, $settings, $options);
    }
    return NULL;
  }

  /**
   * Return the src list.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $settings
   *   The settings.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The src list.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSrcList(MediaInterface $media, array $settings, array $options): array {
    $src_list = [];

    $srcset_list = $settings['srcset'];
    unset($settings['srcset']);

    foreach ($srcset_list as $media_query => $srcset_settings) {
      // Init local settings.
      if ($srcset_settings === 'default') {
        $local_settings = empty($settings) ? $srcset_settings : $settings;
      }
      else {
        $local_settings = $srcset_settings + $settings;
      }

      // Get the image file id.
      $file_id = $this->getFileId($media, $settings, $options);

      // Build srcset.
      $uri = $this->getImageUri($file_id, $local_settings, $options);
      if ($uri) {
        $src_list[] = [
          'uri' => $uri,
          'url' => $this->imageBuilder->getUrl($uri, $options),
          'media_query' => $media_query,
        ];
      }
    }

    return $src_list;
  }

  /**
   * Return the file ID.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $settings
   *   The settings.
   * @param array $options
   *   The options.
   *
   * @return string|null
   *   The file id.
   */
  protected function getFileId(MediaInterface $media, array $settings, array $options): ?string {
    return $settings['fileId'] ?? $this->mediaTwigTools->getFileIdFromMedia($media, $options);
  }

  /**
   * Format size attributes values.
   *
   * @param array $data
   *   The list of data.
   *
   * @return string
   *   The formatted attribute.
   */
  protected function getFormattedSrcSetAttributes(array $data): string {
    $values = [];

    /*
     * If there is only one rule in srcset, and this rule is a pixel ratio one
     * the srcset value should be the 'url' only (without media query) in order
     * to always display one image at least.
     */
    if (count($data) === 1 && $this->isPixelRatioQuery(reset($data))) {
      return reset($data)['url'] ?? '';
    }

    foreach ($data as $value) {
      $values[] = trim($this->imageBuilder->replace(
        '@url @media_query',
        [
          '@url' => $value['url'] ?? '',
          '@media_query' => $value['media_query'] ?? '',
        ]));
    }

    return implode(',', $values);
  }

  /**
   * Return true if the media query is a pixel ratio.
   *
   * @param array $srcset
   *   The srcset.
   *
   * @return bool
   *   True if it is a pixel ratio media query.
   */
  protected function isPixelRatioQuery(array $srcset): bool {
    if (!isset($srcset['media_query'])) {
      // No media query.
      return FALSE;
    }
    if (str_contains($srcset['media_query'], ' ')) {
      // Several rules in media query.
      return FALSE;
    }

    $unit = strtolower(preg_replace('/[^a-zA-Z\s]/', '', $srcset['media_query']));
    return $unit === 'x' || 'default';
  }

}
