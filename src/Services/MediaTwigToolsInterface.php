<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\media\MediaInterface;

/**
 * Provides twig tools for facilitate media integration.
 *
 * - Render the image tag directly :
 *    {{ imgFromMedia(content.field_media_1, {style:'my_style', class:'class1
 * class2', title:'My title', {attributes: {'data-attr-1':'1']}}) }}
 */
interface MediaTwigToolsInterface {

  /**
   * Service name.
   *
   * @const string
   *
   * @deprecated in media_twig_tools:2.1.0 and is removed from media_twig_tools:4.0.0
   *   Use \Drupal:service(MediaTwigToolsInterface::SERVICE_ID);
   *
   * @see https://www.drupal.org/project/media_twig_tools/issues/3369533
   */
  const SERVICE_NAME = 'media_twig_tools.media_twig_tools';

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'media_twig_tools.media_twig_tools';

  /**
   * OPTION style.
   *
   * @const string
   */
  const OPTION_STYLE = 'style';

  /**
   * OPTION nom du champ.
   *
   * @const string
   */
  const OPTION_FIELD_NAME = 'field_name';

  /**
   * OPTION class.
   *
   * @const string
   */
  const OPTION_CLASS = 'class';

  /**
   * OPtion title.
   *
   * @const string
   */
  const OPTION_TITLE = 'title';

  /**
   * OPTION dimension.
   *
   * @const string
   */
  const OPTION_DIMENSION = 'display_dimension';

  /**
   * OPTION Settings.
   *
   * @const string
   */
  const OPTION_SETTINGS = 'settings';

  /**
   * OPTION relative url.
   *
   * @const strings
   */
  const OPTION_RELATIVE = 'relative';

  /**
   * OPTION attribute.
   *
   * @const string
   */
  const OPTION_ATTRIBUTES = 'attributes';

  /**
   * OPTION effects.
   *
   * @const string
   */
  const OPTION_EFFECTS = 'effects';

  /**
   * OPTION no-size.
   *
   * @const string
   */
  const OPTION_NO_SIZE_ATTRIBUTE = 'no_size';

  /**
   * OPTION force allow oversize.
   *
   * @const string
   */
  const OPTION_FORCE_ALLOW_OVERSIZE = 'oversize';

  /**
   * OPTION Override src.
   *
   * @const string
   */
  public const OPTION_OVERRIDE_SRC = 'data-override-src';

  /**
   * OPTION Types fallback list.
   *
   * @const string
   */
  public const OPTION_TYPES_FALLBACK = 'types';

  /**
   * OPTION base effect.
   *
   * @const string
   */
  public const OPTION_BASE_EFFECT = 'base_effect';

  /**
   * Return the default options.
   *
   * @param array $options
   *   The user options.
   *
   * @return array
   *   THe merged options
   */
  public function getDefaultOptions(array $options);

  /**
   * Return an image build array from a media.
   *
   * @param mixed $media_params
   *   The media or the media build array.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The image build array.
   */
  public function imgFromMedia($media_params, array $options = []): array;

  /**
   * Return the list of media entities from a twig parameter.
   *
   * @param mixed $media_params
   *   The media params.
   *
   * @return array
   *   The media entity list.
   */
  public function getMediaEntityFromTwigParam($media_params): array;

  /**
   * Return the media url.
   *
   * @param mixed $media_params
   *   THe media.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The media Urls.
   */
  public function getMediaUrl($media_params, array $options = []): array;

  /**
   * Return the file id from media.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   *
   * @return string
   *   The file id.
   */
  public function getFileIdFromMedia(MediaInterface $media, array $options): string;

  /**
   * Build picture tag from media.
   *
   * @param mixed $media_params
   *   The media params from twig.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The build array.
   */
  public function pictureFromMedia($media_params, array $options): array;

  /**
   * Update generated files on crop change.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $crop
   *   The crop entity.
   */
  public function updateCrop(FieldableEntityInterface $crop): void;

  /**
   * Return the media URL with effects token.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param string $token
   *   The token.
   *
   * @return string
   *   The final url.
   */
  public function generateUrlWithToken(MediaInterface $media, string $token): string;

  /**
   * Return the media URL with effects token.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   * @param array $effects
   *   The effects.
   *
   * @return string|null
   *   The final url.
   */
  public function generateUrlWithEffects(MediaInterface $media, array $options = [], array $effects = []): ?string;

  /**
   * Return the list of file extensions by media ID..
   *
   * @param mixed $media_params
   *   The media params.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The list of extensions (by media id).
   */
  public function getExtensions($media_params, array $options = []): array;

}
