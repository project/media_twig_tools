<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\FileInterface;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface;
use Drupal\media_twig_tools\Services\Traits\SettingsTrait;

/**
 * Service description.
 */
class OversizeChecker implements OversizeCheckerInterface {

  use SettingsTrait;

  /**
   * Constructs an OversizeChecker object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface $conf
   *   The configuration.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected MediaTwigToolsConfigInterface $conf,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getOversizeFilteredSettings(?FileInterface $file = NULL, array $settings = []) {
    try {
      $dimension = $this->getOriginalFileDimension($file);
      if ($dimension) {
        // Filter effects.
        $settings[MediaTwigToolsInterface::OPTION_EFFECTS] = array_filter(
          $this->getEffects($settings),
          function ($effect) use ($dimension) {
            return !$this->isEffectOversize($effect, $dimension);
          });
      }
    }
    catch (\Exception $error) {
      return $settings;
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function isEffectOversize(array $effect, array $original_dimension) {
    if (isset($effect['data'])) {
      $effect_data = $effect['data'];
      $checklist = array_intersect_key($effect_data, $original_dimension);
      $oversize_dimensions = array_filter(
        $checklist,
        function ($value, $dimension) use ($original_dimension) {
          return $value > $original_dimension[$dimension];
        },
        ARRAY_FILTER_USE_BOTH
      );

      return !empty($oversize_dimensions);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isGeneratedFileOversized(FileInterface $file, array $settings): bool {
    $dimension = $this->getOriginalFileDimension($file);

    if ($dimension) {
      $effects = $this->getEffects($settings);
      foreach ($effects as $effect) {
        if ($this->isEffectOversize($effect, $dimension)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isOversizedProhibited(array $options = [], array $settings = []): bool {
    $allow_oversize = !$this->conf->isOversizeCheckEnable();
    if (isset($settings[MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE])) {
      $allow_oversize = $settings[MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE];
    }
    elseif (isset($options[MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE])) {
      $allow_oversize = $options[MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE];
    }
    return !$allow_oversize;
  }

  /**
   * Return the file dimensions.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   *
   * @return array|null
   *   The dimensions if file exists.
   */
  protected function getOriginalFileDimension(FileInterface $file): ?array {
    return $this->getImageFileDimensions($file->getFileUri());
  }

  /**
   * Return the image file dimensions if exists.
   *
   * @param string $file_uri
   *   The file uri.
   *
   * @return array|null
   *   The dimensions
   */
  public function getImageFileDimensions(string $file_uri): ?array {
    if (file_exists($file_uri)) {
      $info = getimagesize($file_uri);
      if ($info) {
        return array_combine(['width', 'height'], array_splice($info, 0, 2));
      }
    }
    return NULL;
  }

}
