<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media_twig_tools\Entity\MediaTwigToolsEffectInterface;

/**
 * Service description.
 */
class EffectsManager implements EffectsManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|null
   */
  protected ?EntityStorageInterface $storage;

  /**
   * Constructs a EffectsManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    if ($entityTypeManager->hasDefinition('media_twig_tools_effect')) {
      $this->storage = $entityTypeManager->getStorage('media_twig_tools_effect');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEffectToken(array $definition): ?string {
    $effect = $this->getEffect(['effect_definition' => $this->serialize($definition)]);
    return $effect?->id() ?? NULL;
  }

  /**
   * Return a effect by its definition.
   *
   * @param array $properties
   *   The selector properties.
   *
   * @return \Drupal\media_twig_tools\Entity\MediaTwigToolsEffectInterface|null
   *   The effect.
   */
  protected function getEffect(array $properties): ?MediaTwigToolsEffectInterface {
    $effects = $this->storage?->loadByProperties($properties) ?? [];
    return empty($effects) ? NULL : reset($effects);
  }

  /**
   * {@inheritdoc}
   */
  public function createEffectToken(array $definition): string {
    $token = $this->getEffectToken($definition);
    if (!$token) {
      $token = $this->generateTokenId($definition);
      $effect = $this->storage?->create(
        [
          'token' => $token,
          'effect_definition' => $this->serialize($definition),
        ]
      );
      $effect?->save();
      $token = $effect?->id() ?? $token;
    }

    return $token;
  }

  /**
   * Generate token for definition.
   *
   * @param array $definition
   *   The definition.
   *
   * @return string
   *   The token.
   */
  protected function generateTokenId(array $definition) {
    $serialized = $this->serialize($definition);
    return $this->getMainEffectDimensionPrefix($definition) . md5($serialized);
  }

  /**
   * Return a readable string according to effect main size.
   *
   * @param array $definition
   *   The definition.
   *
   * @return string
   *   The main size if exists.
   */
  protected function getMainEffectDimensionPrefix(array $definition): string {
    $dimension = $this->getMainEffectDimension($definition);

    $value = ($dimension['width'] ?? NULL) . 'x' . ($dimension['height'] ?? NULL);
    if ($value != 'x') {
      return $value . '_';
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getMainEffectDimension(array $definition = []): ?array {
    foreach (array_reverse($definition) as $effect) {
      if (isset($effect['data']['width']) || isset($effect['data']['height'])) {
        return [
          'width' => $effect['data']['width'] ?? NULL,
          'height' => $effect['data']['height'] ?? NULL,
        ];
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getEffectsByToken(string $token): ?array {
    $effect = $this->getEffect(['token' => $token]);

    $value = $effect?->effect_definition?->value ?? $effect?->effect_definition;
    return $this->deserialize($value) ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function serialize(array $effects = []): string {
    return json_encode($effects);
  }

  /**
   * {@inheritdoc}
   */
  public function deserialize(?string $raw): ?array {
    $data = NULL;
    try {
      if ($raw) {
        $data = json_decode($raw, TRUE);
      }
    }
    catch (\Exception $e) {
    }
    return $data;
  }

}
