<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;

/**
 * Service description.
 */
interface GeneratedFilesManagerInterface {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'media_twig_tools.generated_files_manager';

  /**
   * Return the list of generated files from the media.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   *
   * @return array
   *   The files path.
   */
  public function getGeneratedFilesFromMedia(MediaInterface $media): array;

  /**
   * Return the list of generated files from the file id.
   *
   * @param string $file_id
   *   The file id.
   *
   * @return array
   *   The files path.
   */
  public function getGeneratedFilesFromFileId(string $file_id): array;

  /**
   * Return all referenced file ids of a media.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   *
   * @return array
   *   The file ids.
   */
  public function getReferencedFileIds(MediaInterface $media): array;

  /**
   * Clear generated files of the media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media.
   */
  public function clearGeneratedFilesFromMedia(MediaInterface $media): void;

  /**
   * Clear generated files of the file entity.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   */
  public function clearGeneratedFilesFromFile(FileInterface $file): void;

  /**
   * Return the list of dirs containing generated images.
   *
   * @param bool $force
   *   Must force cache rebuild.
   *
   * @return array
   *   The list of directories.
   */
  public function getDirs($force = FALSE): array;

  /**
   * Return space base directory.
   *
   * @param string $space
   *   The space (ex. public).
   *
   * @return false|string
   *   The real path.
   */
  public function getSpaceBaseDir(string $space): bool|string;

  /**
   * Delete directory.
   *
   * @param string $dirname
   *   The directory name.
   * @param callable|null $callback
   *   The callback.
   *
   * @return string[]
   *   The list of deleted files.
   */
  public function cleanDir(string $dirname, ?callable $callback = NULL): array;

}
