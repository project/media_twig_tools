<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * MediaTwigToolsFormatter plugin manager.
 */
class MediaTwigToolsFormatterPluginManager extends DefaultPluginManager implements MediaTwigToolsFormatterPluginManagerInterface {

  /**
   * Instances cache.
   *
   * @var \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterInterface[]
   */
  protected array $instances = [];

  /**
   * Constructs MediaTwigToolsFormatterPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/MediaTwigToolsFormatter',
      $namespaces,
      $module_handler,
      'Drupal\media_twig_tools\Services\MediaTwigToolsFormatterInterface',
      'Drupal\media_twig_tools\Annotation\MediaTwigToolsFormatter'
    );
    $this->alterInfo('media_twig_tools_formatter_info');
    $this->setCacheBackend($cache_backend, 'media_twig_tools_formatter_plugins');
  }

  /**
   * Return formatter by id.
   *
   * @param string $id
   *   The formatter id.
   *
   * @return \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterInterface|null
   *   The formatter.
   */
  public function getFormatter(string $id): ?MediaTwigToolsFormatterInterface {
    if (!isset($this->instances[$id])) {
      $configuration = $this->getDefinition($id);
      $this->instances[$id] = isset($configuration) ? $this->createInstance($id, $configuration) : NULL;
    }

    return $this->instances[$id];
  }

}
