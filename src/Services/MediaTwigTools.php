<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\file\FileInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Provides twig tools for facilitate media integration.
 *
 * - Render the image tag directly :
 *    {{ imgFromMedia(content.field_media_1, {style:'my_style', class:'class1
 * class2', title:'My title', {attributes: {'data-attr-1':'1']}}) }}
 */
class MediaTwigTools extends AbstractExtension implements MediaTwigToolsInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\media_twig_tools\Services\ImageBuilderInterface $imageBuilder
   *   The image builder.
   * @param \Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface $conf
   *   The configuration.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\media_twig_tools\Services\OversizeCheckerInterface $oversizeChecker
   *   The oversize checker.
   * @param \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface $formatterManager
   *   The formatter plugin manager.
   * @param \Drupal\media_twig_tools\Services\GeneratedFilesManagerInterface $generatedFilesManager
   *   The generated files manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LanguageManagerInterface $languageManager,
    protected ImageBuilderInterface $imageBuilder,
    protected MediaTwigToolsConfigInterface $conf,
    protected FileSystemInterface $fileSystem,
    protected OversizeCheckerInterface $oversizeChecker,
    protected MediaTwigToolsFormatterPluginManagerInterface $formatterManager,
    protected GeneratedFilesManagerInterface $generatedFilesManager,
  ) {
  }

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   *
   * @deprecated in media_twig_tools:2.1.0 and is removed from media_twig_tools:4.0.0
   *   Use \Drupal:service(MediaTwigToolsInterface::SERVICE_ID);
   *
   * @see https://www.drupal.org/project/media_twig_tools/issues/3369533
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_ID);
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('getMediaUrl', $this->getMediaUrl(...)),
      new TwigFunction('getExtensions', $this->getExtensions(...)),
      new TwigFunction('imgFromMedia', $this->imgFromMedia(...)),
      new TwigFunction('pictureFromMedia', $this->pictureFromMedia(...)),
      new TwigFunction('pictureFallbackFromMedia', $this->pictureFallbackFromMedia(...)),
    ];
  }

  /**
   * Return the default options.
   *
   * @param array $options
   *   The user options.
   *
   * @return array
   *   THe merged options
   */
  public function getDefaultOptions(array $options) {
    $style = $options[static::OPTION_STYLE] ?? NULL;

    $default_options = [
      static::OPTION_STYLE => $style,
      static::OPTION_CLASS => $options[static::OPTION_CLASS] ?? NULL,
      static::OPTION_FIELD_NAME => $options[static::OPTION_FIELD_NAME] ?? 'field_media_image',
      static::OPTION_TITLE => $options[static::OPTION_TITLE] ?? NULL,
      static::OPTION_SETTINGS => $options[static::OPTION_SETTINGS] ?? [],
      static::OPTION_RELATIVE => $options[static::OPTION_RELATIVE] ?? TRUE,
      static::OPTION_ATTRIBUTES => $options[static::OPTION_ATTRIBUTES] ?? [],
      static::OPTION_NO_SIZE_ATTRIBUTE => $options[static::OPTION_NO_SIZE_ATTRIBUTE] ?? TRUE,
    ];
    $options = $default_options + $options;

    // Init base effect recursively.
    $options['settings'] = $this->initSettings(
      $options['settings'],
      $options[MediaTwigToolsInterface::OPTION_BASE_EFFECT] ?? NULL,
      $options[MediaTwigToolsInterface::OPTION_STYLE] ?? NULL,
    );

    return $options;
  }

  /**
   * Complete the sub settings according to parents base effect.
   *
   * @param array $raw_settings
   *   The raw settings.
   * @param array|null $base_effect
   *   The parent base effect.
   * @param string|null $base_style
   *   The base style.
   *
   * @return array
   *   The final settings.
   */
  protected function initSettings(array $raw_settings = [], ?array $base_effect = [], ?string $base_style = NULL): array {
    $base_effect = $raw_settings[MediaTwigToolsInterface::OPTION_BASE_EFFECT] ?? $base_effect;
    $base_style = $raw_settings[MediaTwigToolsInterface::OPTION_STYLE] ?? $base_style;

    $settings_list = $raw_settings;
    foreach ($settings_list as &$settings) {
      if ($settings === 'default') {
        // Init default settings.
        $settings = [self::OPTION_STYLE => $base_style ?? []];
      }
      else {
        // Init base and sub settings.
        $base_effect = $settings[MediaTwigToolsInterface::OPTION_BASE_EFFECT] ?? $base_effect;
        if (isset($settings['data'])) {
          $settings[static::OPTION_EFFECTS] = [array_merge($base_effect ?? [], ['data' => $settings['data']])];
          unset($settings['data']);
        }
        elseif (is_array($settings)) {
          foreach ($settings as $key => &$sub_settings) {
            if ($key !== MediaTwigToolsInterface::OPTION_EFFECTS && is_array($sub_settings)) {
              $sub_settings = $this->initSettings($sub_settings, $base_effect);
            }
          }
        }
      }
    }
    return $settings_list;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaEntityFromTwigParam($media_params): array {
    $media_list = [];
    if (is_array($media_params)) {
      foreach (Element::children($media_params) as $key) {
        if ($media_params[$key] instanceof Media) {
          // List of raw media.
          $media_list[] = $media_params[$key];
        }
        elseif ($media_params[$key]['#media'] instanceof Media) {
          // List de build array.
          $media_list[] = $media_params[$key]['#media'];
        }
      }
    }
    elseif ($media_params instanceof Media) {
      $media_list = [$media_params];
    }
    elseif ($media_params instanceof EntityReferenceFieldItemList) {
      $media_list = $media_params->referencedEntities();
    }
    elseif ($media_params instanceof EntityReferenceItem) {
      $media_list = [
        $this->entityTypeManager->getStorage('media')
          ->load($media_params->target_id),
      ];
    }

    // Manage translations.
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    return array_map(
      function (MediaInterface $media) use ($langcode) {
        return $media->hasTranslation($langcode) ? $media->getTranslation($langcode) : $media;
      },
      $media_list
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaUrl($media_params, array $options = []): array {
    $options = $this->getDefaultOptions($options);

    $urls = [];
    foreach ($this->getMediaEntityFromTwigParam($media_params) as $media) {
      $uri = $this->imageBuilder->getStyledUriFromFileId(
        $this->getFileIdFromMedia($media, $options),
        $this->imageBuilder->getStyle($options[static::OPTION_STYLE]),
      );
      $urls[$media->id()] = $this->imageBuilder->getUrl($uri, $options);
    }

    return $urls;
  }

  /**
   * Return the file id from media.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   *
   * @return string
   *   The file id.
   */
  public function getFileIdFromMedia(MediaInterface $media, array $options): string {
    return $media->get($options[static::OPTION_FIELD_NAME])?->target_id;
  }

  /**
   * Return an image build array from a media.
   *
   * @param mixed $media_params
   *   The media or the media build array.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The image build array.
   */
  public function imgFromMedia($media_params, array $options = []): array {
    $options = $this->getDefaultOptions($options);

    $build_array = [];
    $builder = $this->formatterManager->getFormatter('image');
    foreach ($this->getMediaEntityFromTwigParam($media_params) as $media) {
      if ($build = $builder->build($media, $options)) {
        $build_array[] = $build;
      }
    }

    return $build_array;
  }

  /**
   * Build picture tag from media.
   *
   * @param mixed $media_params
   *   The media params from twig.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The build array.
   */
  public function pictureFromMedia($media_params, array $options): array {
    $options = $this->getDefaultOptions($options);
    $picture = [];
    $picture_builder = $this->formatterManager->getFormatter('simple_picture');
    foreach ($this->getMediaEntityFromTwigParam($media_params) as $media) {
      $picture[] = $picture_builder->build($media, $options);
    }

    return $picture;
  }

  /**
   * Build picture tag with fallback formats from media.
   *
   * @param mixed $media_params
   *   The media params from twig.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The build array.
   */
  public function pictureFallbackFromMedia($media_params, array $options): array {
    $options = $this->getDefaultOptions($options);
    $picture = [];
    $picture_builder = $this->formatterManager->getFormatter('types_fallback_picture');

    foreach ($this->getMediaEntityFromTwigParam($media_params) as $media) {
      $picture[] = $picture_builder->build($media, $options);
    }

    return $picture;
  }

  /**
   * {@inheritdoc}
   */
  public function updateCrop(FieldableEntityInterface $crop): void {
    if ($crop->get('entity_type')?->value === 'file') {
      $file = $this->entityTypeManager->getStorage('file')->load($crop->get('entity_id')?->value);
      if ($file) {
        $this->generatedFilesManager->clearGeneratedFilesFromFile($file);
      }
    }
  }

  /**
   * Return the media URL with effects token.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param string $token
   *   The token.
   *
   * @return string
   *   The final url.
   */
  public function generateUrlWithToken(MediaInterface $media, string $token): string {
    $options = $this->getDefaultOptions(
      [
        MediaTwigToolsInterface::OPTION_RELATIVE => FALSE,
        MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE => TRUE,
      ]);

    try {
      if (str_contains($token, 'x')) {
        $data = explode('x', $token);
        switch (count($data)) {
          case 2:
            [$width, $height] = $data;
            break;

          default:
            [$width, $height, $style] = $data;
        }

        $options = $this->getDefaultOptions(
          [
            MediaTwigToolsInterface::OPTION_RELATIVE => FALSE,
            MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE => TRUE,
            MediaTwigToolsInterface::OPTION_STYLE => $style ?? NULL,
          ]);

        // Get the image file id.
        $file_id = $this->getFileIdFromMedia($media, $options);
        $uri = $this->imageBuilder->getScaledAndCroppedUri($file_id, $width, $height, $options);

        if ($uri) {
          $url = $this->imageBuilder->getUrl($uri, $options);
        }
      }
      else {
        throw new \Exception('no effects');
      }
    }
    catch (\Exception $e) {
      $url = NULL;
    }

    if (!isset($url)) {
      $urls = $this->getMediaUrl($media, $options);
      $url = reset($urls);
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function generateUrlWithEffects(MediaInterface $media, array $options = [], array $effects = []): ?string {
    $options = $this->getDefaultOptions($options);

    $file_id = $this->getFileIdFromMedia($media, $options);
    $file = $this->entityTypeManager->getStorage('file')->load($file_id);
    if ($file instanceof FileInterface) {
      $uri = $this->imageBuilder->getUriWithEffectsFromMedia(
        $file,
        $effects,
        $options
      );

      if ($uri) {
        return $this->imageBuilder->getUrl($uri, $options);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensions($media_params, array $options = []): array {
    $media_list = $this->getMediaEntityFromTwigParam($media_params);
    $file_storage = $this->entityTypeManager->getStorage('file');
    $extensions = [];
    foreach ($media_list as $media) {
      $file_id = $this->getFileIdFromMedia($media, $this->getDefaultOptions($options));
      if ($file_id) {
        /** @var ?\Drupal\file\Entity\File $file */
        $file = $file_storage->load($file_id);
        $extensions[$media->id()] = $file?->getMimeType();
      }
    }
    return $extensions;
  }

}
