<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\file\FileInterface;
use Drupal\image\ImageStyleInterface;

/**
 * This class builds image from settings.
 */
interface ImageBuilderInterface {

  /**
   * Service name.
   *
   * @const string
   *
   * @deprecated in media_twig_tools:2.1.0 and is removed from media_twig_tools:4.0.0
   *   Use \Drupal:service(MediaTwigToolsInterface::SERVICE_ID);
   *
   * @see https://www.drupal.org/project/media_twig_tools/issues/3369533
   */
  const SERVICE_NAME = 'media_twig_tools.image_builder';

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'media_twig_tools.image_builder';

  /**
   * Effects separator.
   *
   * @const string
   */
  const EFFECTS_SEPARATOR = '@';

  /**
   * Return the id for http cache.
   *
   * @return string
   *   The cache id parameter.
   */
  public function getCacheId(): string;

  /**
   * Return the url image from settings.
   *
   * @param string $file_id
   *   The file id.
   * @param array $settings
   *   The settings.
   * @param array $options
   *   The options.
   *
   * @return string
   *   The uri.
   */
  public function getImageUriFromSettings(string $file_id, array $settings, array $options): ?string;

  /**
   * Quick replace.
   *
   * @param string $source
   *   The source string.
   * @param array $replace
   *   The replacement [$needle => $replacement].
   *
   * @return array|string|string[]
   *   The string replaced.
   */
  public function replace($source, array $replace);

  /**
   * Return the effects from the dirname.
   *
   * @param string $path
   *   THe path.
   *
   * @return array
   *   The effects.
   */
  public function getEffectsFromPathString($path);

  /**
   * Return settings with style.
   *
   * @param array $srcSettings
   *   The src settings.
   *
   * @return array
   *   The settings with style.
   */
  public function getSettingsWithStyle(array $srcSettings);

  /**
   * Return the dirname according to effects data.
   *
   * @param array $effectsData
   *   The effects data.
   *
   * @return string
   *   The effects dirname.
   */
  public function getEffectsDirnameFromEffectsData(array $effectsData = []);

  /**
   * Return true if $path is an image builder route.
   *
   * @param string $path
   *   THe path.
   *
   * @return bool
   *   The status of the image.
   */
  public function isImageBuilderRoute(string $path);

  /**
   * Return the build image dirname.
   *
   * @return string
   *   THe media dirname.
   */
  public function getMediaDirname();

  /**
   * Return the no style name.
   *
   * @return string
   *   THe no style name.
   */
  public function getNoStyleName();

  /**
   * Generate the image and return the generator.
   *
   * @param string $path
   *   The image path.
   *
   * @return \Drupal\media_twig_tools\Generators\ImageGenerator
   *   The Image Generator.
   *
   * @throws \Exception
   */
  public function generateImage(string $path);

  /**
   * Return image builder data from image path.
   *
   * @param string $path
   *   The path.
   *
   * @return array
   *   The data.
   *
   * @throws \Exception
   */
  public function getDataFromPath($path);

  /**
   * Return the url image from file and settings.
   *
   * @param string $file_id
   *   The file id.
   * @param string $uri
   *   The uri.
   * @param array $settings
   *   The source settings.
   * @param array $options
   *   The options.
   *
   * @return string|null
   *   The image path.
   */
  public function getImageUrlFromFileAndSettings(string $file_id, string $uri, array $settings, array $options): ?string;

  /**
   * Scale and crop image.
   *
   * @param string $file_id
   *   The file id.
   * @param int $width
   *   The width.
   * @param int $height
   *   The height.
   * @param array $options
   *   The options.
   *
   * @return array|string|string[]|null
   *   The url.
   */
  public function getScaledAndCroppedUri(string $file_id, int $width, int $height, array $options = []);

  /**
   * Return url with effects.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   * @param array $effects
   *   THe effects.
   * @param array $options
   *   The options.
   *
   * @return array|string|string[]|null
   *   The url.
   */
  public function getUriWithEffectsFromMedia(FileInterface $file, array $effects, array $options = []);

  /**
   * Build original image.s.
   *
   * @param \Drupal\file\FileInterface|null $file
   *   The file.
   */
  public function buildOriginalImage(?FileInterface $file = NULL): void;

  /**
   * Build the image file.
   *
   * @param string $uri
   *   The uri.
   */
  public function buildImageFile(string $uri): void;

  /**
   * Return the styled url from file Id.
   *
   * @param string $file_id
   *   THe file id.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The style.
   *
   * @return string|null
   *   The file uri.
   */
  public function getStyledUriFromFileId(string $file_id, ?ImageStyleInterface $style = NULL): ?string;

  /**
   * Return the real path of the file from its uri.
   *
   * @param string $uri
   *   The uri.
   * @param string $space
   *   The space.
   *
   * @return string
   *   The real path.
   */
  public function getFileRealPathFromUri(string $uri, string $space = 'public'): string;

  /**
   * Return the style according to settings or options.
   *
   * @param array|string|null $array_1
   *   The array 1.
   * @param array $array_2
   *   The array 2.
   * @param array $array_3
   *   The array 3.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   The style.
   */
  public function getStyle(array|string|null $array_1 = [], array $array_2 = [], array $array_3 = []): ?ImageStyleInterface;

  /**
   * Generate an url from the uri.
   *
   * @param string $uri
   *   The input uri.
   * @param array|null $options
   *   The options.
   * @param bool|null $absolute
   *   Force absolute if true.
   *
   * @return string
   *   The url.
   */
  public function getUrl(string $uri, ?array $options = [], ?bool $absolute = NULL): string;

}
