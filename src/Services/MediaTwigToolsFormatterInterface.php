<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\media\MediaInterface;

/**
 * Interface for media_twig_tools_formatter plugins.
 */
interface MediaTwigToolsFormatterInterface {

  /**
   * Return a build array display a fallback type picture element.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The build array.
   */
  public function build(MediaInterface $media, array $options): array;

}
