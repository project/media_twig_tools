<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\media\MediaInterface;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface;

/**
 * Service description.
 */
class GeneratedFilesManager implements GeneratedFilesManagerInterface {

  /**
   * Directories list.
   *
   * @var array
   */
  protected array $dirs = [];

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   */
  public static function instance() {
    return \Drupal::service(static::SERVICE_ID);
  }

  /**
   * Constructs a GeneratedFilesManagerInterface object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface $conf
   *   The media twig tools conf.
   */
  public function __construct(
    protected FileSystemInterface $fileSystem,
    protected MediaTwigToolsConfigInterface $conf,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function clearGeneratedFilesFromMedia(MediaInterface $media): void {
    foreach ($this->getGeneratedFilesFromMedia($media) as $file) {
      $this->fileSystem->delete($file->uri);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function clearGeneratedFilesFromFile(FileInterface $file): void {
    foreach ($this->getGeneratedFilesFromFileId($file->id()) as $file) {
      $this->fileSystem->delete($file->uri);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getGeneratedFilesFromMedia(MediaInterface $media): array {
    $files = [];
    foreach ($this->getReferencedFileIds($media) as $file_id) {
      $files = array_merge($files, $this->getGeneratedFilesFromFileId($file_id));
    }

    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function getGeneratedFilesFromFileId(string $file_id): array {
    $files = [];
    foreach ($this->getDirs() as $sub_dirs_list) {
      foreach ($sub_dirs_list as $dir) {
        $files += array_filter($this->fileSystem->scanDirectory($dir, '/^' . $file_id . '_/'));
      }
    }
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function getSpaceBaseDir(string $space): bool|string {
    return $this->fileSystem->realpath($space . '://');
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencedFileIds(MediaInterface $media): array {
    $files = [];

    // Get fields referring a file item list.
    $file_fields = array_filter(
      $media->toArray(),
      function ($field_name) use ($media) {
        return str_starts_with($field_name, 'field_') && $media->get($field_name) instanceof FileFieldItemList;
      },
      ARRAY_FILTER_USE_KEY
    );

    // Flatten ids.
    foreach ($file_fields as $values) {
      $files = array_merge(array_column($values, 'target_id'), $files);
    }

    return array_unique($files);
  }

  /**
   * {@inheritdoc}
   */
  public function getDirs($force = FALSE): array {
    if ($force || empty($this->dirs)) {
      // Init spaces.
      $this->dirs = [];
      foreach (['public', 'private'] as $scheme) {
        // @phpstan-ignore-next-line
        if (\Drupal::hasService('stream_wrapper.' . $scheme)) {
          $this->dirs[$scheme] = NULL;
        }
      }
      $dirname = $this->conf->getDirname();

      foreach ($this->dirs as $scheme => &$dir) {
        if (!is_dir($scheme . '://')) {
          continue;
        }
        // Prepare subdirectory patterns.
        $scheme_path = $this->fileSystem->realpath($scheme . '://');

        $patterns = [
          $scheme_path . '/styles/' . $dirname . '/*',
        ];

        foreach ($patterns as $pattern) {
          $paths = glob($pattern);
          if (!empty($paths)) {
            $dir = array_merge($dir ?? [], $paths);
          }
        }
      }

      $this->dirs = array_filter($this->dirs);
    }

    return $this->dirs;
  }

  /**
   * {@inheritdoc}
   */
  public function cleanDir(string $dirname, ?callable $callback = NULL): array {
    $files = [];
    $this->fileSystem->deleteRecursive($dirname, function ($item) use (&$files) {
      if (!is_dir($item)) {
        $files[] = $item;
      }
    });

    return $files;
  }

}
