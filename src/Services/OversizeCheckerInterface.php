<?php

namespace Drupal\media_twig_tools\Services;

use Drupal\file\FileInterface;

/**
 * Service description.
 */
interface OversizeCheckerInterface {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'media_twig_tools.oversize_checker';

  /**
   * Return settings without oversize effects.
   *
   * @param \Drupal\file\FileInterface|null $file
   *   The file.
   * @param array $settings
   *   The settings.
   *
   * @return array
   *   The filtered settings.
   */
  public function getOversizeFilteredSettings(?FileInterface $file = NULL, array $settings = []);

  /**
   * Check if effect is oversize.
   *
   * @param array $effect
   *   The effect data.
   * @param array $original_dimension
   *   The original dimension.
   *
   * @return bool
   *   True if oversize.
   */
  public function isEffectOversize(array $effect, array $original_dimension);

  /**
   * Return true if the final generated file will be bigger than the original.
   *
   * @return bool
   *   True if the final generated file will be bigger than the original file.
   */
  public function isGeneratedFileOversized(FileInterface $file, array $settings): bool;

  /**
   * Return true if the oversize is prohibited.
   *
   * @param array $options
   *   The options.
   * @param array $settings
   *   The settings.
   *
   * @return bool
   *   The oversize prohibition state.
   */
  public function isOversizedProhibited(array $options = [], array $settings = []): bool;

  /**
   * Return the image file dimensions if exists.
   *
   * @param string $file_uri
   *   The file uri.
   *
   * @return array|null
   *   The dimensions
   */
  public function getImageFileDimensions(string $file_uri): ?array;

}
