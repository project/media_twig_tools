<?php

namespace Drupal\media_twig_tools\Services\Traits;

/**
 * Provides a simple replace method.
 */
trait ReplaceTrait {

  /**
   * Quick replace.
   *
   * @param string $source
   *   The source string.
   * @param array $replace
   *   The replacement [$needle => $replacement].
   *
   * @return array|string|string[]
   *   The string replaced.
   */
  public function replace($source, array $replace) {
    return str_replace(
      array_keys($replace),
      array_values($replace),
      $source,
    );
  }

}
