<?php

namespace Drupal\media_twig_tools\Services\Traits;

use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;

/**
 * Provide settings trait.
 */
trait SettingsTrait {

  /**
   * Return the effects in the settings array.
   *
   * @param array $settings
   *   The settings.
   *
   * @return array
   *   The list of effects.
   */
  public function getEffects(array $settings): array {
    if (isset($settings[MediaTwigToolsInterface::OPTION_EFFECTS])) {
      return $settings[MediaTwigToolsInterface::OPTION_EFFECTS];
    }
    // Compatibility.
    if (isset($settings['rules'])) {
      return $settings['rules'];
    }

    return [];
  }

  /**
   * Return the local options.
   *
   * @param array $options
   *   The options.
   *
   * @return array
   *   The local settings.
   */
  public function getLocalSettings(array $options): array {
    $local_settings = [];
    // Init local style.
    if (isset($options[MediaTwigToolsInterface::OPTION_STYLE])) {
      $local_settings[MediaTwigToolsInterface::OPTION_STYLE] = $options[MediaTwigToolsInterface::OPTION_STYLE];
    }

    // Init local base effect.
    if (isset($options[MediaTwigToolsInterface::OPTION_BASE_EFFECT])
      && isset($options[MediaTwigToolsInterface::OPTION_BASE_EFFECT]['data'])) {
      $local_settings[MediaTwigToolsInterface::OPTION_EFFECTS] = [
        0 => $options[MediaTwigToolsInterface::OPTION_BASE_EFFECT],
      ];
    }

    return $local_settings;
  }

}
