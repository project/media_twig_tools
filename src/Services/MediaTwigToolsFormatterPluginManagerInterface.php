<?php

namespace Drupal\media_twig_tools\Services;

/**
 * MediaTwigToolsFormatter plugin manager.
 */
interface MediaTwigToolsFormatterPluginManagerInterface {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'plugin.manager.media_twig_tools_formatter';

  /**
   * Return formatter by id.
   *
   * @param string $id
   *   The formatter id.
   *
   * @return \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterInterface|null
   *   The formatter.
   */
  public function getFormatter(string $id): ?MediaTwigToolsFormatterInterface;

}
