<?php

namespace Drupal\media_twig_tools\Services;

/**
 * Retrieve and create effects definition.
 */
interface EffectsManagerInterface {

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'media_twig_tools.effects_manager';

  /**
   * Retrieve the token of a effects set.
   *
   * @param array $definition
   *   The effect definition.
   *
   * @return string|null
   *   The effect token.
   */
  public function getEffectToken(array $definition): ?string;

  /**
   * Create the token of a effects set.
   *
   * @param array $definition
   *   The effect definition.
   *
   * @return string
   *   The effect token.
   */
  public function createEffectToken(array $definition): string;

  /**
   * Retrieve effects by token.
   *
   * @param string $token
   *   The token.
   *
   * @return array|null
   *   The effects if exists.
   */
  public function getEffectsByToken(string $token): ?array;

  /**
   * Serialize effect.
   *
   * @param array $effects
   *   The effects.
   *
   * @return string
   *   The data.
   */
  public function serialize(array $effects = []): string;

  /**
   * Deserialize effect.
   *
   * @param string|null $raw
   *   The serialized data.
   *
   * @return array|null
   *   The effects.
   */
  public function deserialize(?string $raw): ?array;

  /**
   * Return the main effect dimension array.
   *
   * @param array $definition
   *   The effects list.
   *
   * @return array|null
   *   The main effect dimension.
   */
  public function getMainEffectDimension(array $definition = []): ?array;

}
