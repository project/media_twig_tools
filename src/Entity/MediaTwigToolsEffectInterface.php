<?php

namespace Drupal\media_twig_tools\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a Media Twig Tools Effect entity type.
 */
interface MediaTwigToolsEffectInterface extends ContentEntityInterface {

}
