<?php

namespace Drupal\media_twig_tools\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Media Twig Tools Effect entity class.
 *
 * @ContentEntityType(
 *   id = "media_twig_tools_effect",
 *   label = @Translation("Media Twig Tools Effect"),
 *   label_collection = @Translation("Media Twig Tools Effects"),
 *   label_singular = @Translation("Media Twig Tools Effect"),
 *   label_plural = @Translation("Media Twig Tools Effects"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Media Twig Tools Effects",
 *     plural = "@count Media Twig Tools Effects",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\media_twig_tools\Entity\MediaTwigToolsEffectAccessControlHandler",
 *     "form" = {
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     }
 *   },
 *   base_table = "media_twig_tools_effect",
 *   admin_permission = "administer Media Twig Tools Effect",
 *   entity_keys = {
 *     "id" = "token",
 *   },
 *   links = {
 *     "delete-form" = "/admin/media-twig-tools-effect/{media_twig_tools_effect}/delete",
 *   },
 * )
 */
class MediaTwigToolsEffect extends ContentEntityBase implements MediaTwigToolsEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];
    $fields[$entity_type->getKey('id')] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Token'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['effect_definition'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Effect definition'))
      ->setDescription(t('Image effects (json).'))
      ->setDefaultValue('');

    return $fields;
  }

}
