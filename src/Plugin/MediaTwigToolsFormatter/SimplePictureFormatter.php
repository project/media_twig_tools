<?php

namespace Drupal\media_twig_tools\Plugin\MediaTwigToolsFormatter;

use Drupal\media\MediaInterface;
use Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginBase;
use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;

/**
 * Build simple picture elements.
 *
 * @MediaTwigToolsFormatter(
 *   id = "simple_picture",
 * )
 */
class SimplePictureFormatter extends MediaTwigToolsFormatterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isValid(MediaInterface $media, array $options): bool {
    $valid = TRUE;
    if (!isset($options[MediaTwigToolsInterface::OPTION_SETTINGS])) {
      $this->addValidationError('The "' . MediaTwigToolsInterface::OPTION_SETTINGS . '" must be defined and must contain settings for each source.');
      $valid = FALSE;
    }

    return $valid;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkup(MediaInterface $media, array $options): array {
    // Init the build array.
    $build = [
      '#theme' => 'picture',
    ];

    // Init main attributes.
    $this->initImagesClasses($build, $options);

    // Init main image tag.
    $image_formatter = $this->formatterManager->getFormatter('image');
    $build['img'] = $image_formatter->build($media, $options);

    // Init sources.
    $build['sources'] = [];
    foreach ($options[MediaTwigToolsInterface::OPTION_SETTINGS] as $mediaQuery => $settings) {
      $build['sources'][] = $this->buildSource($media, $mediaQuery, NULL, $settings, $options);
    }

    return $build;
  }

}
