<?php

namespace Drupal\media_twig_tools\Plugin\MediaTwigToolsFormatter;

use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginBase;
use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;
use Drupal\media_twig_tools\Services\Traits\SettingsTrait;

/**
 * Build image element.
 *
 * @MediaTwigToolsFormatter(
 *   id = "image",
 * )
 */
class ImageFormatter extends MediaTwigToolsFormatterPluginBase {

  use SettingsTrait;

  /**
   * {@inheritdoc}
   */
  protected function isValid(MediaInterface $media, array $options): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkup(MediaInterface $media, array $options): array {

    // Get the file.
    $files = $media->get($options[MediaTwigToolsInterface::OPTION_FIELD_NAME])
      ->referencedEntities();
    /** @var \Drupal\file\FileInterface $file */
    $file = reset($files);

    if (!$file) {
      return [];
    }

    // Get the render of the main item of the media.
    $formatter_build = $media->get($options[MediaTwigToolsInterface::OPTION_FIELD_NAME])
      ->first()
      ->view();

    // Init data storage.
    $formatter_build['#media_data'] = [];

    // Redefine meta data.
    $this->initImageMeta($formatter_build, $media, $options);

    // Manage classes.
    $this->initImagesClasses($formatter_build, $options);

    // Manage image tag.
    $this->initImageTag($formatter_build);

    // Init attributes.
    $this->initAttributes($formatter_build, $options);

    // Manage image style.
    $this->initImageFormatterStyleData($formatter_build, $file, $options);

    // Init src.
    $this->initSrc($formatter_build, $media, $options);

    // Init srcset.
    $this->initSrcSet($formatter_build, $media, $options);

    // Init image size attributes.
    $this->initImageSizeAttributes($formatter_build, $media, $file, $options);

    return $formatter_build;
  }

  /**
   * Initialize image meta data.
   *
   * @param array $formatter_build
   *   The image formatter build array.
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function initImageMeta(array &$formatter_build, MediaInterface $media, array $options) {
    $image_data = $media->get($options[MediaTwigToolsInterface::OPTION_FIELD_NAME])
      ->first()
      ->toArray();
    $formatter_build['#alt'] = empty($image_data['alt']) ? $media->label() : $image_data['alt'];

    // Title.
    if (isset($options[MediaTwigToolsInterface::OPTION_TITLE]) || !empty($image_data['title'])) {
      $formatter_build['#item_attributes']['title'] = $options[MediaTwigToolsInterface::OPTION_TITLE] ?? $image_data['title'];
    }
  }

  /**
   * Return the image style data.
   *
   * @param array $formatter_build
   *   The image formatter build array.
   * @param \Drupal\file\FileInterface $file
   *   The file.
   * @param array $options
   *   The options.
   */
  protected function initImageFormatterStyleData(array &$formatter_build, FileInterface $file, array $options) {
    /** @var \Drupal\image\ImageStyleInterface $style */
    $style = $this->imageBuilder->getStyle($options[MediaTwigToolsInterface::OPTION_STYLE]);
    if ($style) {
      $formatter_build['#image_style'] = $style->id();
    }
  }

  /**
   * Add base data to the image formatter.
   *
   * @param array $formatter_build
   *   The image formatter build array.
   */
  protected function initImageTag(array &$formatter_build) {
    $formatter_build['#item_attributes']['class'] = array_key_exists(
      '#attributes',
      $formatter_build
    ) ? $formatter_build['#attributes']['class'] : [];
    $formatter_build['#item_attributes']['alt'] = $formatter_build['#alt'];
  }

  /**
   * Init image attributes.
   *
   * @param array $formatter_build
   *   The image formatter build array.
   * @param array $options
   *   The options.
   */
  protected function initAttributes(array &$formatter_build, array $options) {
    $formatter_build['#item_attributes'] = $options[MediaTwigToolsInterface::OPTION_ATTRIBUTES] + $formatter_build['#item_attributes'];
  }

  /**
   * Init image sizes attributes.
   *
   * @param array $formatter_build
   *   The image formatter build array.
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param \Drupal\file\FileInterface $file
   *   The file.
   * @param array $options
   *   The options.
   *
   * @throws \Exception
   */
  protected function initImageSizeAttributes(array &$formatter_build, MediaInterface $media, FileInterface $file, array $options): void {
    // Do not display size attributes.
    if ($options[MediaTwigToolsInterface::OPTION_NO_SIZE_ATTRIBUTE]) {
      $formatter_build['#item_attributes'][MediaTwigToolsInterface::OPTION_NO_SIZE_ATTRIBUTE] = 1;
      return;
    }

    $main_size = $this->getMainImageDimensions($formatter_build);
    if ($main_size) {
      $formatter_build['#item_attributes']['width'] = $main_size['width'];
      $formatter_build['#item_attributes']['height'] = $main_size['height'];
    }
  }

  /**
   * Return the main image dimensions.
   *
   * @param array $formatter_build
   *   The formatter array.
   *
   * @return array|null
   *   The dimensions
   *
   * @throws \Exception
   */
  protected function getMainImageDimensions(array &$formatter_build): ?array {
    // If image has srcset, first try to get srcset first size.
    if ($formatter_build['#media_data']['srcset']) {
      $main_rule = reset($formatter_build['#media_data']['srcset']);
      return $this->getImageDimensions($main_rule['url'], $main_rule['uri'], TRUE);
    }

    $src = $formatter_build['#media_data']['src'];
    return $this->getImageDimensions($src['url'], $src['uri']);
  }

  /**
   * Return the dimensions according to the url and uri.
   *
   * @param string $url
   *   The file url.
   * @param string $uri
   *   The file uri.
   * @param bool $force
   *   Force file build if necessary.
   *
   * @return array|null
   *   The dimensions.
   *
   * @throws \Exception
   */
  public function getImageDimensions(string $url, string $uri, bool $force = FALSE): ?array {
    $image_data = $this->imageBuilder->getDataFromPath($url);

    // If any dimensions are defined in the effects list.
    $dimensions = $this->effectsManager->getMainEffectDimension($image_data['effects']);
    if ($dimensions) {
      return $dimensions;
    }

    // If no dimensions are defined.
    $dimensions = $this->oversizeChecker->getImageFileDimensions($uri);
    if ($dimensions) {
      return $dimensions;
    }

    if ($force) {
      /*
       * Here the dimensions are still not reachable because the file is not
       * generated yet. So we try to generate it by calling the url.
       */
      $this->imageBuilder->buildImageFile($uri);
      // If no dimensions are defined.
      return $this->oversizeChecker->getImageFileDimensions($uri);
    }
    return NULL;
  }

  /**
   * Initialize src set attributes for images.
   *
   * @param array $formatter_build
   *   The image formatter build array.
   * @param \Drupal\media\Entity\Media $media
   *   The media.
   * @param array $options
   *   The options.
   */
  protected function initSrcSet(array &$formatter_build, MediaInterface $media, array $options) {
    $srcset = $this->getSrcSetValues($media, $options, $options);
    $formatter_build['#media_data']['srcset'] = $srcset;

    if (!empty($srcset)) {
      if (count($srcset) === 1) {
        $main_srcset = reset($srcset);
        if ($this->isPixelRatioQuery($main_srcset)) {
          $formatter_build['#item_attributes'][MediaTwigToolsInterface::OPTION_OVERRIDE_SRC] = $main_srcset['url'];
        }
        if ($formatter_build['#item_attributes'][MediaTwigToolsInterface::OPTION_OVERRIDE_SRC] === $main_srcset['url']) {
          /*
           * Delete srcset if only one srcset is declared and
           * is the same as the src.
           */
          return;
        }
      }

      $formatter_build['#item_attributes']['srcset'] = $this->getFormattedSrcSetAttributes($srcset);
    }
  }

  /**
   * Init default src image.
   */
  protected function initSrc(array &$formatter_build, MediaInterface $media, array $options) {
    // Store raw data.
    $formatter_build['#media_data']['src'] = [];

    // Init basic settings.
    $local_settings = $this->getLocalSettings($options);

    // Force oversize for main src in order to always display an image.
    $local_settings[MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE] = TRUE;

    $uri = $this->getImageUri(
      $this->mediaTwigTools->getFileIdFromMedia($media, $options),
      $local_settings,
      $options
    );

    if ($uri) {
      $formatter_build['#media_data']['src'] = [
        'uri' => $uri,
        'url' => $this->imageBuilder->getUrl($uri),
      ];

      $formatter_build['#item_attributes'][MediaTwigToolsInterface::OPTION_OVERRIDE_SRC] = $formatter_build['#media_data']['src']['url'];
    }
  }

}
