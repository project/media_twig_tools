<?php

namespace Drupal\media_twig_tools\Plugin\MediaTwigToolsFormatter;

use Drupal\image\ImageStyleInterface;
use Drupal\media\MediaInterface;
use Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginBase;
use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;
use Drupal\media_twig_tools\Services\Traits\SettingsTrait;

/**
 * Build simple picture elements.
 *
 * @MediaTwigToolsFormatter(
 *   id = "types_fallback_picture",
 * )
 */
class TypesFallbackPictureFormatter extends MediaTwigToolsFormatterPluginBase {

  use SettingsTrait;

  /**
   * Type default.
   *
   * @const string
   */
  public const DEFAULT_TYPE = 'default';

  /**
   * Type query field.
   *
   * @const string
   */
  public const DEFAULT_QUERY = 'default';

  /**
   * {@inheritdoc}
   */
  protected function isValid(MediaInterface $media, array $options): bool {
    $valid = TRUE;
    if (!isset($options[MediaTwigToolsInterface::OPTION_SETTINGS][static::DEFAULT_QUERY])) {
      $this->addValidationError('The "' . static::DEFAULT_QUERY . '" settings must be defined to display image not corresponding to any media query.');
      $valid = FALSE;
    }
    if (!isset($options[MediaTwigToolsInterface::OPTION_TYPES_FALLBACK])) {
      $this->addValidationError('The "' . MediaTwigToolsInterface::OPTION_TYPES_FALLBACK . '" options must define the image styles fallback priority list.');
      $valid = FALSE;
    }

    return $valid;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkup(MediaInterface $media, array $options): array {
    // Init the build array.
    $build = [
      '#theme' => 'picture',
    ];

    // Init main attributes.
    $this->initImagesClasses($build, $options);

    // Init main image tag.
    $image_formatter = $this->formatterManager->getFormatter('image');
    $build['img'] = $image_formatter->build($media, $options);

    // Init sources.
    $fallback_data = $this->getFallbackTypeData($media, $options);
    $build['sources'] = $fallback_data['sources'];

    // Init sources.
    return $build;
  }

  /**
   * Return the list of fallback data.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param array $options
   *   The options.
   *
   * @return array
   *   The list of sources
   */
  protected function getFallbackTypeData(MediaInterface $media, array $options): array {
    // Complete base effects.
    $settings_list = $options[MediaTwigToolsInterface::OPTION_SETTINGS];

    // Foreach valid types.
    $types = $this->getValidTypes($options);

    // Generate type urls.
    $build = [
      'sources' => [],
      'default' => NULL,
    ];
    foreach ($types as $type) {
      foreach ($settings_list as $media_query => $settings) {
        $settings = $this->getEffectsWithFormatTypeSettings($type, $settings);
        $source = $this->buildSource($media, $media_query, $type, $settings, $options);

        if (!empty($source)) {
          $build['sources'][] = $source;
        }
        else {
          $build['default'] = $this->getFormattedSrcSetAttributes($this->getSrcSetValues($media, $settings, $options));
        }
      }
    }

    return $build;
  }

  /**
   * Return the valid types only.
   *
   * @param array $options
   *   The options.
   *
   * @return array
   *   The valid types {ImageStyleInterface|string}.
   */
  protected function getValidTypes(array $options) {
    return array_filter(
      array_map(
        fn($type) => $this->imageBuilder->getStyle($type) ?: ($type === static::DEFAULT_TYPE ? static::DEFAULT_TYPE : NULL),
        $options[MediaTwigToolsInterface::OPTION_TYPES_FALLBACK]
      ));
  }

  /**
   * Return the final format settings effects.
   *
   * @param string|\Drupal\image\ImageStyleInterface $type
   *   The type.
   * @param array $settings
   *   The settings.
   *
   * @return array
   *   The formatted effects.
   */
  protected function getEffectsWithFormatTypeSettings(string|ImageStyleInterface $type, array $settings = []): array {
    if ($type instanceof ImageStyleInterface) {
      $effects = $this->getEffects($settings);
      $settings[MediaTwigToolsInterface::OPTION_EFFECTS] = array_merge($effects, $type->getEffects()->getConfiguration());

      if (isset($settings['srcset'])) {
        foreach ($settings['srcset'] as $rule => $srcset) {
          $effects = $this->getEffects($srcset);
          $settings['srcset'][$rule][MediaTwigToolsInterface::OPTION_EFFECTS] = array_merge($effects, $type->getEffects()->getConfiguration());
        }
      }
    }

    return $settings;
  }

}
