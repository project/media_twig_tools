<?php

namespace Drupal\media_twig_tools\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a Delete effect action.
 *
 * @Action(
 *   id = "effect_definition_delete",
 *   label = @Translation("Delete effect"),
 *   type = "media_twig_tools_effect",
 *   category = @Translation("Media Twig Tools")
 * )
 */
class DeleteEffect extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access = $object->access('delete', $account, TRUE);
    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if (isset($entity)) {
      try {
        $entity->delete();
      }
      catch (EntityStorageException $e) {
        $this->messenger()
          ->addError('Error while deleting custom entity: ' .
                     $e->getMessage());
      }
    }
  }

}
