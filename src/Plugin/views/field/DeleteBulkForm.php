<?php

namespace Drupal\media_twig_tools\Plugin\views\field;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a user operations bulk form element.
 *
 * @ViewsField("media_twig_tools_effect_delete_bulk_form")
 */
class DeleteBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage(): string|TranslatableMarkup {
    return $this->t('Nothing selected.');
  }

}
