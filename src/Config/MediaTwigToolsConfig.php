<?php

namespace Drupal\media_twig_tools\Config;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Configuration for MediaTwigTools settings.
 */
class MediaTwigToolsConfig implements MediaTwigToolsConfigInterface {

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $conf;

  /**
   * Editable config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $editableConf;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {
    $this->conf = $this->configFactory->get(static::SERVICE_ID);
  }

  /**
   * Return the editable config.
   *
   * @return \Drupal\Core\Config\Config
   *   The editable config.
   */
  protected function getEditable() {
    if (!isset($this->editableConf)) {
      $this->editableConf = $this->configFactory->getEditable(static::SERVICE_ID);
    }
    return $this->editableConf;
  }

  /**
   * Singleton quick access.
   *
   * @return static
   *   Singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_ID);
  }

  /**
   * {@inheritdoc}
   */
  public function getDirname() {
    return $this->conf->get(static::FIELD_DIRNAME) ?: 'builder';
  }

  /**
   * {@inheritdoc}
   */
  public function setDirname($dirname) {
    $this->getEditable()
      ->set(static::FIELD_DIRNAME, $dirname)
      ->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNoStyle() {
    return $this->conf->get(static::FIELD_NO_STYLE) ?: static::DEFAULT_NO_STYLE_IDENTIFIER;
  }

  /**
   * {@inheritdoc}
   */
  public function setNoStyle($noStyle) {
    $this->getEditable()
      ->set(static::FIELD_NO_STYLE, $noStyle)
      ->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowMemoryLimit() {
    return $this->conf->get(static::FIELD_ALLOW_MEMORY_LIMIT);
  }

  /**
   * {@inheritdoc}
   */
  public function setAllowMemoryLimit($allow_memory_limit) {
    $this->getEditable()
      ->set(static::FIELD_ALLOW_MEMORY_LIMIT, $allow_memory_limit)
      ->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isFormatCheckEnable() {
    return $this->conf->get(static::FIELD_ENABLE_FORMAT_CHECK) ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function enableFormatCheck($enable = TRUE) {
    $this->getEditable()
      ->set(static::FIELD_ENABLE_FORMAT_CHECK, $enable)
      ->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isOversizeCheckEnable() {
    return $this->conf->get(static::FIELD_ENABLE_OVERSIZE_CHECK) ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function enableOversizeCheck($enable = TRUE) {
    $this->getEditable()
      ->set(static::FIELD_ENABLE_OVERSIZE_CHECK, $enable)
      ->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isOldEffectsFormatEnable() {
    return $this->conf->get(static::FIELD_ALLOW_OLD_EFFECTS_FORMAT) ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function enableOldEffectsFormat($enable = TRUE) {
    $this->getEditable()
      ->set(static::FIELD_ALLOW_OLD_EFFECTS_FORMAT, $enable)
      ->save();

    return $this;
  }

}
