<?php

namespace Drupal\media_twig_tools\Config;

/**
 * Configuration for MediaTwigTools settings.
 */
interface MediaTwigToolsConfigInterface {

  /**
   * Service name.
   *
   * @const string
   *
   * @deprecated in media_twig_tools:2.1.0 and is removed from media_twig_tools:4.0.0
   *   Use \Drupal:service(MediaTwigToolsInterface::SERVICE_ID);
   *
   * @see https://www.drupal.org/project/media_twig_tools/issues/3369533
   */
  public const SERVICE_NAME = 'media_twig_tools.config';

  /**
   * Service ID.
   *
   * @const string
   */
  public const SERVICE_ID = 'media_twig_tools.config';

  /**
   * Field dirname.
   *
   * @const string
   */
  public const FIELD_DIRNAME = 'dirname';

  /**
   * Field no style.
   *
   * @const string
   */
  public const FIELD_NO_STYLE = 'no_style';

  /**
   * Default no style identifier.
   *
   * @const string
   */
  public const DEFAULT_NO_STYLE_IDENTIFIER = 'unstyle';

  /**
   * Allow memory limit field.
   *
   * @const string
   */
  public const FIELD_ALLOW_MEMORY_LIMIT = 'allow_memory_limit';

  /**
   * Enable format check.
   *
   * @const string
   */
  public const FIELD_ENABLE_FORMAT_CHECK = 'enable_format_check';

  /**
   * Enable Oversize check.
   *
   * @const string
   */
  public const FIELD_ENABLE_OVERSIZE_CHECK = 'enable_oversize_check';

  /**
   * Allow old effects format.
   *
   * @const string
   */
  public const FIELD_ALLOW_OLD_EFFECTS_FORMAT = 'allow_old_effects_format';

  /**
   * Return the dirname.
   *
   * @return string
   *   The dirname.
   */
  public function getDirname();

  /**
   * Update the dirname.
   *
   * @param string $dirname
   *   The dirname.
   *
   * @return MediaTwigToolsConfig
   *   The config.
   */
  public function setDirname($dirname);

  /**
   * Return the no style identifier.
   *
   * @return string
   *   The no style identifier.
   */
  public function getNoStyle();

  /**
   * Update the no style identifier.
   *
   * @param string $noStyle
   *   The no style identifier.
   *
   * @return MediaTwigToolsConfig
   *   The config.
   */
  public function setNoStyle($noStyle);

  /**
   * Return the allowed memory limit.
   *
   * @return string|null
   *   The allowed memory limit.
   */
  public function getAllowMemoryLimit();

  /**
   * Update the allowed max size.
   *
   * @param string $allow_memory_limit
   *   The allowed memory limit.
   *
   * @return MediaTwigToolsConfig
   *   The config.
   */
  public function setAllowMemoryLimit($allow_memory_limit);

  /**
   * Return the format check feature enable status.
   *
   * @return bool
   *   The format check feature status.
   */
  public function isFormatCheckEnable();

  /**
   * Update the format check feature enable status.
   *
   * @param bool $enable
   *   The status.
   *
   * @return MediaTwigToolsConfig
   *   The config.
   */
  public function enableFormatCheck($enable = TRUE);

  /**
   * Return the oversize check enable status.
   *
   * @return bool
   *   The oversize check feature status.
   */
  public function isOversizeCheckEnable();

  /**
   * Update the oversize check feature status.
   *
   * @param bool $enable
   *   The status.
   *
   * @return MediaTwigToolsConfig
   *   The config.
   */
  public function enableOversizeCheck($enable = TRUE);

  /**
   * Return the old effects format enable status.
   *
   * @return bool
   *   The old effects format status.
   */
  public function isOldEffectsFormatEnable();

  /**
   * Update the old effects format status.
   *
   * @param bool $enable
   *   The status.
   *
   * @return MediaTwigToolsConfig
   *   The config.
   */
  public function enableOldEffectsFormat($enable = TRUE);

}
