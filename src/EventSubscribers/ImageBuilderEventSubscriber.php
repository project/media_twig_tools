<?php

namespace Drupal\media_twig_tools\EventSubscribers;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\media_twig_tools\Services\ImageBuilderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Intercept a 404 response and check if it is due to a none generated Image.
 */
class ImageBuilderEventSubscriber implements EventSubscriberInterface, TrustedCallbackInterface {

  /**
   * Image Builder.
   *
   * @var \Drupal\media_twig_tools\Services\ImageBuilderInterface
   */
  protected ImageBuilderInterface $imageBuilder;

  /**
   * Constructor.
   *
   * @param \Drupal\media_twig_tools\Services\ImageBuilderInterface $imageBuilder
   *   The Image Builder service.
   */
  public function __construct(ImageBuilderInterface $imageBuilder) {
    $this->imageBuilder = $imageBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => 'onResponse',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['onResponse'];
  }

  /**
   * On Kernel response.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $responseEvent
   *   The response event.
   */
  public function onResponse(ResponseEvent $responseEvent) {
    $pathInfo = $responseEvent->getRequest()->getPathInfo();
    if ($this->imageBuilder->isImageBuilderRoute($pathInfo)) {
      try {
        $generator = $this->imageBuilder->generateImage($pathInfo);

        if ($generator->fileExists()) {
          if ($generator->getSpace() === 'public') {
            if ($responseEvent->getResponse()->getStatusCode() === Response::HTTP_NOT_FOUND) {
              // Image exists. Redirect to the same URL that is no more 404.
              $redirectResponse = new RedirectResponse($responseEvent->getRequest()->getPathInfo());
              $redirectResponse->send();
            }
          }
          else {
            $headers = [
              'Content-Type' => $generator->getMimeType(),
              'Content-Length' => $generator->getFileSize(),
            ];
            $response = new BinaryFileResponse($generator->getDestination(), 200, $headers, FALSE);
            $response->send();
          }
        }
      }
      catch (\Exception $exception) {
        // Continue default drupal process...
      }
    }
  }

}
