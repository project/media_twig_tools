<?php

namespace Drupal\media_twig_tools\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines media_twig_tools_formatter annotation object.
 *
 * @Annotation
 */
class MediaTwigToolsFormatter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
