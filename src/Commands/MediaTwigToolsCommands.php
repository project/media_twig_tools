<?php

namespace Drupal\media_twig_tools\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media_twig_tools\Services\GeneratedFilesManager;
use Drush\Commands\DrushCommands;

/**
 * Command for media twig tools interactions.
 */
class MediaTwigToolsCommands extends DrushCommands {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\media_twig_tools\Services\GeneratedFilesManager $builtFilesManager
   *   The built files manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected GeneratedFilesManager $builtFilesManager,
  ) {
  }

  /**
   * Delete all existing effects.
   *
   * @param array $options
   *   Command options.
   *
   * @option delete-files
   *   True will preserve the generate files.
   * @usage media_twig_tools:delete-all-effects --preserve-files
   *   Delete all existing effects.
   *
   * @command media_twig_tools:delete-all-effects
   * @aliases mtt:delete-all-effects
   */
  public function deleteAllEffects($options = ['preserve-files' => FALSE]) {
    $storage = $this->entityTypeManager->getStorage('media_twig_tools_effect');
    $all_effects = $storage->loadMultiple();
    $storage->delete($all_effects);
    $this->logger()->success(dt('@effects effects deleted.', [
      '@effects' => count($all_effects),
    ]));

    if (!$options['preserve-files']) {
      $dirs_tree = $this->builtFilesManager->getDirs();

      $count = 0;
      foreach ($dirs_tree as $dirs) {
        foreach ($dirs as $dir) {
          $count += count($this->builtFilesManager->cleanDir($dir));
        }
      }
      $this->logger()->success(dt('@files files deleted.', ['@files' => $count]));
    }

  }

}
