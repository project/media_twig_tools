<?php

namespace Drupal\Tests\media_twig_tools\Unit;

use Drupal\Tests\media_twig_tools\Unit\Base\AbstractFormatterAssertions;
use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;

/**
 * Test Image formatter generation.
 *
 * @group media_twig_tools
 */
class ImageFormatterTest extends AbstractFormatterAssertions {

  /**
   * Test the no style image simple image with no effect.
   */
  public function testNoStyleNoEffectSimple() {
    $media = $this->getMedia(['id' => 1]);

    $data = $this->getMediaTwigTools()->imgFromMedia($media);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]);
    $this->assertNotHaveSrcset($data[0]['#item_attributes']);
  }

  /**
   * Test the portrait styled image simple image with no effect.
   */
  public function testPortraitNoEffectSimple() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'style' => 'portrait',
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $this->assertMainSrc($this->getExpectedUrl('portrait'), $data[0]);
    $this->assertNotHaveSrcset($data[0]['#item_attributes']);
  }

  /**
   * Test the no styled image simple image with valid simple crop effect.
   */
  public function testNoStyleWithValidSimpleCropEffect() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'base_effect' => [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 200,
          'height' => 200,
        ],
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $token = $this->getEffectsManager()->createEffectToken([$options['base_effect']]);

    $this->assertMainSrc($this->getExpectedUrl(NULL, $token), $data[0]);
    $this->assertNotHaveSrcset($data[0]['#item_attributes']);
  }

  /**
   * Test the no styled image simple image with oversize simple crop effect.
   *
   * The main image should be oversize event if source file is not big enough
   * in order to display an image in the right format in all cases.
   */
  public function testNoStyleWithOversizeSimpleCropEffect() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'base_effect' => [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 400,
          'height' => 400,
        ],
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $token = $this->getEffectsManager()->createEffectToken([$options['base_effect']]);

    $this->assertMainSrc($this->getExpectedUrl(NULL, $token), $data[0]);
    $this->assertNotHaveSrcset($data[0]['#item_attributes']);
  }

  /**
   * Test an image with default and valid src tags.
   *
   * Src 1x should render the default image.
   * Src 2x should render an image in portrait 300x150.
   */
  public function testWithDefaultAndValidSrcTags() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'srcset' => [
        '1x' => 'default',
        '2x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 300,
                'height' => 150,
              ],
            ],
          ],
        ],
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $token2x = $this->getEffectsManager()->createEffectToken($options['srcset']['2x']['effects']);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]);
    $this->assertSrcsetContains('1x', $this->getExpectedUrl(), $data[0]['#item_attributes']);
    $this->assertSrcsetContains('2x', $this->getExpectedUrl('portrait', $token2x), $data[0]['#item_attributes']);
  }

  /**
   * Test an image with two valid src attributes.
   *
   * Src 1x should render an image in portrait 150x75.
   * Src 2x should render an image in portrait 300x150.
   */
  public function testWithValidSrcAttributes() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'srcset' => [
        '1x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 150,
                'height' => 75,
              ],
            ],
          ],
        ],
        '2x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 300,
                'height' => 150,
              ],
            ],
          ],
        ],
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $token1x = $this->getEffectsManager()->createEffectToken($options['srcset']['1x']['effects']);
    $token2x = $this->getEffectsManager()->createEffectToken($options['srcset']['2x']['effects']);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]);
    $this->assertSrcsetContains('1x', $this->getExpectedUrl('portrait', $token1x), $data[0]['#item_attributes']);
    $this->assertSrcsetContains('2x', $this->getExpectedUrl('portrait', $token2x), $data[0]['#item_attributes']);
  }

  /**
   * Test an image with one valid src and one oversize srcset.
   *
   * Src should be the only valid element, because its a density rule.
   * There should not be any srcset.
   */
  public function testWithValidAndInvalidSrcAttributes() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'srcset' => [
        '1x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 300,
                'height' => 150,
              ],
            ],
          ],
        ],
        '2x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 600,
                'height' => 300,
              ],
            ],
          ],
        ],
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $token1x = $this->getEffectsManager()->createEffectToken($options['srcset']['1x']['effects']);

    $this->assertNotHaveSrcset($data[0]['#item_attributes']);
    $this->assertMainSrc($this->getExpectedUrl('portrait', $token1x), $data[0]);
  }

  /**
   * Test an image with 1 valid src and 1 oversize srcset, but allow oversize.
   *
   * There should be 2 srcset tags.
   */
  public function testWithValidAndInvalidSrcAttributesAndAllowOversize() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE => TRUE,
      'srcset' => [
        '1x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 300,
                'height' => 150,
              ],
            ],
          ],
        ],
        '2x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 600,
                'height' => 300,
              ],
            ],
          ],
        ],
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $token1x = $this->getEffectsManager()->createEffectToken($options['srcset']['1x']['effects']);
    $token2x = $this->getEffectsManager()->createEffectToken($options['srcset']['2x']['effects']);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]);
    $this->assertSrcsetContains('1x', $this->getExpectedUrl('portrait', $token1x), $data[0]['#item_attributes']);
    $this->assertSrcsetContains('2x', $this->getExpectedUrl('portrait', $token2x), $data[0]['#item_attributes']);
  }

  /**
   * Test simple image with attributes.
   */
  public function testImageAttributes() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_ATTRIBUTES => [
        'title' => 'Custom title',
        'alt' => 'Custom alternative',
        'data-custom' => 'custom',
        'loading' => 'eager',
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]);
    $this->assertNotHaveSrcset($data[0]['#item_attributes']);
    // Check attributes.
    foreach ($options[MediaTwigToolsInterface::OPTION_ATTRIBUTES] as $name => $value) {
      $this->assertHasAttributeValue($name, $value, $data[0]);
    }
  }

  /**
   * Test simple image with size attributes.
   *
   * Should have width and height equals to main image.
   */
  public function testImageNoSize() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_NO_SIZE_ATTRIBUTE => FALSE,
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]);
    $this->assertNotHaveSrcset($data[0]['#item_attributes']);
    $this->assertNotHasAttribute(MediaTwigToolsInterface::OPTION_NO_SIZE_ATTRIBUTE, $data[0]);
    // Check attributes.
    $this->assertHasAttributeValue('width', 300, $data[0]);
    $this->assertHasAttributeValue('height', 300, $data[0]);
  }

  /**
   * Test simple image with size attributes.
   *
   * Should have width and height equals to the first src dimension.
   */
  public function testImageNoSizeWithSrcSet() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE => TRUE,
      MediaTwigToolsInterface::OPTION_NO_SIZE_ATTRIBUTE => FALSE,
      'srcset' => [
        '1x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 300,
                'height' => 150,
              ],
            ],
          ],
        ],
        '2x' => [
          'style' => 'portrait',
          'effects' => [
            [
              'id' => 'image_scale_and_crop',
              'data' => [
                'width' => 600,
                'height' => 300,
              ],
            ],
          ],
        ],
      ],
    ];

    $data = $this->getMediaTwigTools()->imgFromMedia($media, $options);

    $this->assertHasAttributeValue('width', 300, $data[0]);
    $this->assertHasAttributeValue('height', 150, $data[0]);
  }

}
