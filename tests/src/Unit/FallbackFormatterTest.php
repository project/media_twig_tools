<?php

namespace Drupal\Tests\media_twig_tools\Unit;

use Drupal\Tests\media_twig_tools\Unit\Base\AbstractFormatterAssertions;

/**
 * Test Fallback formatter generation.
 *
 * @group media_twig_tools
 */
class FallbackFormatterTest extends AbstractFormatterAssertions {

  /**
   * Fallback with no effect no style.
   *
   * Should trigger exception.
   */
  public function testFallbackNoTypes() {
    $media = $this->getMedia(['id' => 1]);

    $this->expectException(\Exception::class);
    $this->getMediaTwigTools()->pictureFallbackFromMedia($media, []);
  }

  /**
   * Fallback with no effect no style.
   *
   * Should return one source fallback.
   */
  public function testPictureNoStyleNoEffect() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'types' => [
        'webp', 'default',
      ],
      'settings' => [
        'default' => [],
      ],
    ];

    $conversions = [
      'webp' => [
        'id' => 'image_converter',
        'data' => ['extension' => 'webp'],
      ],
    ];

    $token_default = $this->getEffectsManager()->createEffectToken([]);
    $token_webp = $this->getEffectsManager()->createEffectToken([$conversions['webp']]);

    $data = $this->getMediaTwigTools()->pictureFallbackFromMedia($media, $options);

    // Check default.
    $this->assertMainSrc($this->getExpectedUrl(NULL, $token_default), $data[0]['img']);
    $expected_url = $this->getExpectedUrl(NULL, $token_webp, FALSE, 'png.webp');
    $this->assertHasSourceSrcset($expected_url, $data[0], NULL, 'image/webp');
  }

  /**
   * Fallback with base effect.
   *
   * Should return two fallback sources with effects.
   */
  public function testPictureBaseEffect() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'types' => [
        'webp', 'default',
      ],
      'base_effect' => [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 200,
          'height' => 200,
        ],
      ],
      'settings' => [
        'default' => [
          'data' => [
            'width' => 200,
            'height' => 200,
          ],
        ],
      ],
    ];

    $conversions = [
      'webp' => [
        'id' => 'image_converter',
        'data' => ['extension' => 'webp'],
      ],
    ];

    $token_default = $this->getEffectsManager()->createEffectToken([$options['base_effect']]);
    $token_webp = $this->getEffectsManager()->createEffectToken([$options['base_effect'], $conversions['webp']]);

    $data = $this->getMediaTwigTools()->pictureFallbackFromMedia($media, $options);

    // Check default.
    $this->assertMainSrc($this->getExpectedUrl(NULL, $token_default), $data[0]['img']);
    $expected_url = $this->getExpectedUrl(NULL, $token_webp, FALSE, 'png.webp');
    $this->assertHasSourceSrcset($expected_url, $data[0], NULL, 'image/webp');
  }

  /**
   * Fallback with base effect and media queries.
   *
   * Should return 4 sources with 2 fallbacks types and 2 media query each.
   */
  public function testPictureMediaQuery() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'types' => [
        'webp', 'default',
      ],
      'base_effect' => [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 200,
          'height' => 200,
        ],
      ],
      'settings' => [
        'default' => [
          'id' => 'image_scale_and_crop',
          'data' => [
            'width' => 200,
            'height' => 200,
          ],
        ],
        '(max-width: 1024px)' => [
          'data' => [
            'width' => 50,
            'height' => 50,
          ],
        ],
      ],
    ];

    $conversions = [
      'webp' => [
        'id' => 'image_converter',
        'data' => ['extension' => 'webp'],
      ],
    ];

    $token_default = $this->getEffectsManager()->createEffectToken(
      [
        $options['base_effect'],
      ]
    );
    $token_1024 = $this->getEffectsManager()->createEffectToken(
      [
        array_merge($options['base_effect'], $options['settings']['(max-width: 1024px)']),
      ]
    );
    $token_webp_default = $this->getEffectsManager()->createEffectToken(
      [
        $options['settings']['default'],
        $conversions['webp'],
      ]
    );
    $token_webp_1024 = $this->getEffectsManager()->createEffectToken(
      [
        array_merge($options['base_effect'], $options['settings']['(max-width: 1024px)']),
        $conversions['webp'],
      ]
    );

    $data = $this->getMediaTwigTools()->pictureFallbackFromMedia($media, $options);

    // Check default.
    $this->assertMainSrc($this->getExpectedUrl(NULL, $token_default), $data[0]['img']);

    // Default 1024.
    $expected_url = $this->getExpectedUrl(NULL, $token_1024);
    $this->assertHasSourceSrcset($expected_url, $data[0], '(max-width: 1024px)', 'image/png');

    // Default webp.
    $expected_url = $this->getExpectedUrl(NULL, $token_webp_default, FALSE, 'png.webp');
    $this->assertHasSourceSrcset($expected_url, $data[0], NULL, 'image/webp');

    // Default webp 1024.
    $expected_url = $this->getExpectedUrl(NULL, $token_webp_1024, FALSE, 'png.webp');
    $this->assertHasSourceSrcset($expected_url, $data[0], '(max-width: 1024px)', 'image/webp');
  }

}
