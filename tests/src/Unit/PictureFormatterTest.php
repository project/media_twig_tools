<?php

namespace Drupal\Tests\media_twig_tools\Unit;

use Drupal\Tests\media_twig_tools\Unit\Base\AbstractFormatterAssertions;

/**
 * Test Picture formatter generation.
 *
 * @group media_twig_tools
 */
class PictureFormatterTest extends AbstractFormatterAssertions {

  /**
   * Picture with no effect no style.
   */
  public function testPictureNoStyleNoEffect() {
    $media = $this->getMedia(['id' => 1]);

    $data = $this->getMediaTwigTools()->pictureFromMedia($media, []);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]['img']);
    $this->assertHasNoSource($data[0]['img']['#item_attributes']);
  }

  /**
   * Test picture with base effect.
   *
   * Should have only src with base effect.
   */
  public function testPictureBaseEffect() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'base_effect' => [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 100,
          'height' => 100,
        ],
      ],
    ];

    $token = $this->getEffectsManager()->createEffectToken([$options['base_effect']]);

    $data = $this->getMediaTwigTools()->pictureFromMedia($media, $options);

    $this->assertMainSrc($this->getExpectedUrl(NULL, $token), $data[0]['img']);
    $this->assertHasNoSource($data[0]['img']);
  }

  /**
   * Test picture with sources.
   *
   * Should have one source with 2 srcset attributes.
   */
  public function testPictureSources() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'base_effect' => [
        'id' => 'image_scale_and_crop',
      ],
      'settings' => [
        'default' => [
          'data' => [
            'width' => 100,
            'height' => 100,
          ],
        ],
        '(max-width: 1024px)' => [
          'data' => [
            'width' => 50,
            'height' => 50,
          ],
        ],
      ],
    ];

    $token_default = $this->getEffectsManager()->createEffectToken([array_merge($options['base_effect'], $options['settings']['default'])]);
    $token_1024 = $this->getEffectsManager()->createEffectToken([array_merge($options['base_effect'], $options['settings']['(max-width: 1024px)'])]);

    $data = $this->getMediaTwigTools()->pictureFromMedia($media, $options);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]['img']);
    $this->assertHasSourceSrcset($this->getExpectedUrl(NULL, $token_default), $data[0]);
    $this->assertHasSourceSrcset($this->getExpectedUrl(NULL, $token_1024), $data[0], '(max-width: 1024px)');
  }

  /**
   * Test picture with custom effects.
   *
   * Should have one source with 2 srcset attributes.
   * The custom effect should not extend the base effect.
   */
  public function testPictureSourcesWithCustomEffect() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'base_effect' => [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 300,
          'height' => 300,
        ],
      ],
      'settings' => [
        'default' => [
          'effects' => [
            [
              'id' => 'image_scale',
              'data' => [
                'width' => 100,
                'height' => 100,
              ],
            ],
          ],
        ],
      ],
    ];

    $token_default = $this->getEffectsManager()->createEffectToken($options['settings']['default']['effects']);

    $data = $this->getMediaTwigTools()->pictureFromMedia($media, $options);

    $this->assertHasSourceSrcset($this->getExpectedUrl(NULL, $token_default), $data[0]);
  }

  /**
   * Test picture with sources.
   *
   * Should have two sources.
   * One source should have 2 srcset.
   */
  public function testPictureSourcesWithSrcset() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'base_effect' => [
        'id' => 'image_scale_and_crop',
      ],
      'settings' => [
        'default' => [
          'data' => [
            'width' => 100,
            'height' => 100,
          ],
        ],
        '(max-width: 1024px)' => [
          'srcset' => [
            '1x' => [
              'data' => [
                'width' => 50,
                'height' => 50,
              ],
            ],
            "2x" => [
              'data' => [
                'width' => 100,
                'height' => 100,
              ],
            ],
          ],
        ],
      ],
    ];

    $token_default = $this->getEffectsManager()->createEffectToken([array_merge($options['base_effect'], $options['settings']['default'])]);
    $token_1024_1x = $this->getEffectsManager()->createEffectToken([array_merge($options['base_effect'], $options['settings']['(max-width: 1024px)']['srcset']['1x'])]);
    $token_1024_2x = $this->getEffectsManager()->createEffectToken([array_merge($options['base_effect'], $options['settings']['(max-width: 1024px)']['srcset']['2x'])]);

    $data = $this->getMediaTwigTools()->pictureFromMedia($media, $options);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]['img']);
    $this->assertHasSourceSrcset($this->getExpectedUrl(NULL, $token_default), $data[0]);
    $this->assertHasSourceAllSrcset(
      [
        '2x' => $this->getExpectedUrl(NULL, $token_1024_2x),
        '1x' => $this->getExpectedUrl(NULL, $token_1024_1x),
      ],
      '(max-width: 1024px)', $data[0]);
  }

  /**
   * Test picture with sources and oversize.
   *
   * Should have two sources.
   * One source should have srcset bu.
   */
  public function testPictureSourcesWithSrcsetOversize() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      'base_effect' => [
        'id' => 'image_scale_and_crop',
      ],
      'settings' => [
        'default' => [
          'data' => [
            'width' => 100,
            'height' => 100,
          ],
        ],
        '(max-width: 1024px)' => [
          'srcset' => [
            '1x' => [
              'data' => [
                'width' => 50,
                'height' => 50,
              ],
            ],
            "2x" => [
              'data' => [
                'width' => 2000,
                'height' => 2000,
              ],
            ],
          ],
        ],
      ],
    ];

    $token_default = $this->getEffectsManager()->createEffectToken([array_merge($options['base_effect'], $options['settings']['default'])]);
    $token_1024_1x = $this->getEffectsManager()->createEffectToken([array_merge($options['base_effect'], $options['settings']['(max-width: 1024px)']['srcset']['1x'])]);

    $data = $this->getMediaTwigTools()->pictureFromMedia($media, $options);

    $this->assertMainSrc($this->getExpectedUrl(), $data[0]['img']);
    $this->assertHasSourceSrcset($this->getExpectedUrl(NULL, $token_default), $data[0]);
    /*
     * 2x rule is over size, so it should not appear in the srcset.
     * 1x is the only one media query and is ratio, so it should not be
     * mentioned as well.
     */
    $this->assertHasSourceAllSrcset(
      [
        '' => $this->getExpectedUrl(NULL, $token_1024_1x),
      ],
      '(max-width: 1024px)', $data[0]);
  }

}
