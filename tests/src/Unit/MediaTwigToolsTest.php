<?php

namespace Drupal\Tests\media_twig_tools\Unit;

use Drupal\Tests\media_twig_tools\Unit\Base\AbstractFormatterAssertions;
use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;

/**
 * Test media twig tools misc methods test.
 *
 * @group media_twig_tools
 */
class MediaTwigToolsTest extends AbstractFormatterAssertions {

  /**
   * Test url generation with simple effects.
   *
   * Should give an url with 200x200 effect.
   */
  public function testGenerateUrl() {
    $media = $this->getMedia(['id' => 1]);

    $options = [];
    $effects = [
      [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 200,
          'height' => 200,
        ],
      ],
    ];

    $url = $this->getMediaTwigTools()->generateUrlWithEffects($media, $options, $effects);

    $token = $this->getEffectsManager()->createEffectToken($effects);
    $expected = $this->getExpectedUrl(NULL, $token);

    $this->assertEquals($url, $expected);
  }

  /**
   * Test url generation with simple effects and absolute url.
   *
   * Should give an url with 200x200 effect.
   */
  public function testGenerateAbsoluteUrl() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_RELATIVE => FALSE,
    ];
    $effects = [
      [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 200,
          'height' => 200,
        ],
      ],
    ];

    $url = $this->getMediaTwigTools()->generateUrlWithEffects($media, $options, $effects);

    $token = $this->getEffectsManager()->createEffectToken($effects);
    $expected = $this->getExpectedUrl(NULL, $token, TRUE);

    $this->assertEquals($url, $expected);
  }

  /**
   * Test url generation with simple effects and disallowed over sizing.
   *
   * Should not give an url.
   */
  public function testGenerateNoOversizeUrl() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_RELATIVE => FALSE,
    ];
    $effects = [
      [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 1200,
          'height' => 1200,
        ],
      ],
    ];

    $url = $this->getMediaTwigTools()->generateUrlWithEffects($media, $options, $effects);

    $this->assertNull($url);
  }

  /**
   * Test url generation with simple effects and allowed over sizing.
   *
   * Should give an url.
   */
  public function testGenerateOversizeUrl() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE => TRUE,
    ];
    $effects = [
      [
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => 2000,
          'height' => 2000,
        ],
      ],
    ];

    $url = $this->getMediaTwigTools()->generateUrlWithEffects($media, $options, $effects);

    $token = $this->getEffectsManager()->createEffectToken($effects);
    $expected = $this->getExpectedUrl(NULL, $token);

    $this->assertEquals($url, $expected);
  }

  /**
   * Test get media extensions.
   */
  public function testGetExtension() {
    $media = $this->getMedia(['id' => 1]);

    $options = [
      MediaTwigToolsInterface::OPTION_FORCE_ALLOW_OVERSIZE => TRUE,
    ];

    $extensions = $this->getMediaTwigTools()->getExtensions($media, $options);

    $this->assertEquals([1 => 'image/png'], $extensions);
  }

}
