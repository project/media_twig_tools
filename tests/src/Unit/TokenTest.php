<?php

namespace Drupal\Tests\media_twig_tools\Unit;

use Drupal\Tests\media_twig_tools\Unit\Base\AbstractFormatterAssertions;

/**
 * Test media twig tools token generator.
 *
 * @group media_twig_tools
 */
class TokenTest extends AbstractFormatterAssertions {

  /**
   * Test token generation with scale and crop.
   *
   * Should give an url with 200x200 effect.
   */
  public function testToken() {
    $media = $this->getMedia(['id' => 1]);

    $data = $this->getMediaTwigTools()->generateUrlWithToken($media, '200x200');

    $effect = [
      'id' => 'image_scale_and_crop',
      'data' => [
        'width' => 200,
        'height' => 200,
      ],
    ];
    $token = $this->getEffectsManager()->createEffectToken([$effect]);
    $expected = $this->getExpectedUrl(NULL, $token, TRUE);

    $this->assertEquals($data, $expected);
  }

  /**
   * Test token generation with scale and crop on small media.
   *
   * Should give an url with 2000x2000 effect even if source is 300x300.
   */
  public function testTokenOversize() {
    $media = $this->getMedia(['id' => 1]);

    $data = $this->getMediaTwigTools()->generateUrlWithToken($media, '2000x2000');

    $effect = [
      'id' => 'image_scale_and_crop',
      'data' => [
        'width' => 2000,
        'height' => 2000,
      ],
    ];
    $token = $this->getEffectsManager()->createEffectToken([$effect]);
    $expected = $this->getExpectedUrl(NULL, $token, TRUE);

    $this->assertEquals($data, $expected);
  }

  /**
   * Test token generation with scale and crop and conversion.
   *
   * Should give an url with 200x200 and jpg conversion effect.
   */
  public function testTokenConversion() {
    $media = $this->getMedia(['id' => 1]);

    $format = implode('x', ['200', '200', 'jpg']);
    $data = $this->getMediaTwigTools()->generateUrlWithToken($media, $format);

    $this->assertIsString($data);
    $this->assertStringContainsString(".jpg", $data);
  }

}
