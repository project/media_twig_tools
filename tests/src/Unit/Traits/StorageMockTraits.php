<?php

namespace Drupal\Tests\media_twig_tools\Unit\Traits;

/**
 * Several methods to mock storage.
 */
trait StorageMockTraits {

  use FieldMockTraits;

  /**
   * Entities storages.
   *
   * @var array
   */
  protected array $entitiesStorage = [];


  /**
   * List of mocks.
   *
   * @var array
   */
  protected array $storageList = [];

  /**
   * Create a mock storage.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array|null $mock_data
   *   The mock data (method and class).
   *
   * @return \PHPUnit\Framework\MockObject\MockObject|null
   *   The storage mock.
   */
  protected function createStorageMock(string $entity_type, ?array $mock_data) {
    if (!isset($this->storageList[$entity_type])) {
      $method = $mock_data['method'] ?? NULL;
      if (!is_callable([$this, $method])) {
        return NULL;
      }

      $storage = $this->createMock($mock_data['class']);

      // Store entity storage.
      if (!isset($this->entitiesStorage[$entity_type])) {
        $this->entitiesStorage[$entity_type] = [];
      }

      // Add mock methods.
      $this->addMockCallable($storage, 'load',
        function ($id) use ($method, $entity_type) {
          if (isset($this->entitiesStorage[$entity_type][$id])) {
            return $this->entitiesStorage[$entity_type][$id];
          }
          return call_user_func([$this, $method], ['id' => $id]);
        });
      $this->addMockCallable($storage, 'save',
        function ($id) {
        });
      $this->addMockCallable($storage, 'create',
        function ($data) use ($method, $entity_type) {
          $entity = call_user_func([$this, $method], $data);
          $this->entitiesStorage[$entity_type][$entity->id()] = $entity;

          return $entity;
        });
      $this->addMockCallable($storage, 'loadByProperties',
        function ($data) use ($entity_type) {
          $matching_entities = [];
          if (isset($this->entitiesStorage[$entity_type])) {
            foreach ($this->entitiesStorage[$entity_type] as $entity) {
              $same = TRUE;
              foreach ($data as $key => $value) {
                $same = $same && $entity->get($key) === $value;
              }

              if ($same) {
                $matching_entities[] = $entity;
              }
            }
          }
          return $matching_entities;
        });

      $this->storageList[$entity_type] = $storage;
    }

    return $this->storageList[$entity_type];

  }

}
