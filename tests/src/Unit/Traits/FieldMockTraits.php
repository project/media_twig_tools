<?php

namespace Drupal\Tests\media_twig_tools\Unit\Traits;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Several methods providing field mock tools.
 */
trait FieldMockTraits {

  /**
   * Object property storage.
   *
   * @var \WeakMap
   */
  protected \WeakMap $propertiesStorage;

  /**
   * Add a method callback to mock object.
   *
   * @param \PHPUnit\Framework\MockObject\MockObject $object
   *   The mock object.
   * @param string $method
   *   The method.
   * @param callable $callback
   *   The callback.
   */
  protected function addMockCallable(MockObject $object, string $method, callable $callback): void {
    $object->expects($this->any())
      ->method($method)
      ->willReturnCallback($callback);
  }

  /**
   * Return an entity referenced field mock.
   *
   * @param mixed $entity
   *   The entity.
   *
   * @return \Drupal\Core\Field\EntityReferenceFieldItemList|\PHPUnit\Framework\MockObject\MockObject
   *   The field mock.
   */
  protected function createEntityReferenceField($entity) {
    $field = $this->createMock(EntityReferenceFieldItemList::class);
    $this->addMockCallable($field, 'referencedEntities',
      function () use ($entity) {
        return [$entity];
      });

    $this->addMockCallable($field, 'first',
      function () use ($entity) {
        return $entity;
      });

    $this->addProperty($field, 'target_id', $entity->id());

    return $field;
  }

  /**
   * Add a property to a mock.
   *
   * @param \PHPUnit\Framework\MockObject\MockObject $object
   *   The object.
   * @param string $property_name
   *   The property name.
   * @param mixed $value
   *   The value.
   */
  protected function addProperty(MockObject $object, string $property_name, mixed $value): void {
    if (!isset($this->propertiesStorage)) {
      $this->propertiesStorage = new \WeakMap();
    }

    // Add a dynamic storage for properties.
    if (!$this->propertiesStorage->offsetExists($object)) {
      $this->propertiesStorage[$object] = [];

      if (method_exists($object, '__get')) {
        $this->addMockCallable($object, '__get',
          function ($property) use ($object) {
            return $this->propertiesStorage[$object][$property] ?? NULL;
          });
      }

      if (method_exists($object, 'get')) {
        $this->addMockCallable($object, 'get',
          function ($property) use ($object) {
            return $this->propertiesStorage[$object][$property] ?? NULL;
          });
      }
    }

    $this->propertiesStorage[$object][$property_name] = $value;
  }

  /**
   * Set a protected property of a mock.
   *
   * @param \PHPUnit\Framework\MockObject\MockObject $object
   *   The mock object.
   * @param string $property
   *   The property name.
   * @param mixed $value
   *   The value.
   */
  public function setProtectedProperty(MockObject $object, string $property, mixed $value): void {
    $reflection = new \ReflectionClass($object);
    $reflection_property = $reflection->getProperty($property);
    $reflection_property->setAccessible(TRUE);
    $reflection_property->setValue($object, $value);
  }

  /**
   * Return a callable method from protected.
   *
   * @param \PHPUnit\Framework\MockObject\MockObject $object
   *   The mock object.
   * @param string $method
   *   The method name.
   *
   * @return \ReflectionMethod
   *   The method.
   */
  public function getCallableProtectedMethod(MockObject $object, string $method): \ReflectionMethod {
    $reflection = new \ReflectionClass($object);
    $reflection_method = $reflection->getMethod($method);
    $reflection_method->setAccessible(TRUE);
    return $reflection_method;
  }

}
