<?php

namespace Drupal\Tests\media_twig_tools\Unit\Traits;

use Drupal\Core\DependencyInjection\Container;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Several methods to mock drupal container.
 */
trait ContainerMockTraits {

  /**
   * The container mock.
   *
   * @var \Drupal\Core\DependencyInjection\Container|\PHPUnit\Framework\MockObject\MockObject
   */
  protected Container|MockObject $container;

  /**
   * Return mock container.
   *
   * @return \Drupal\Component\DependencyInjection\ContainerInterface
   *   The container.
   */
  protected function getContainerMock() {
    if (!isset($this->container)) {
      $available_containers = method_exists($this, 'getAvailableServicesMap') ? $this->getAvailableServicesMap() : [];

      $this->container = $this->createMock(Container::class);
      $this->addMockCallable($this->container, 'get',
        function ($id) use ($available_containers) {
          if (isset($available_containers[$id]) && method_exists($this, $available_containers[$id])) {
            return $this->{$available_containers[$id]}();
          }
          return NULL;
        });
    }
    return $this->container;
  }

}
