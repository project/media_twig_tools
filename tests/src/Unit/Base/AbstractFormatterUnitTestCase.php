<?php

namespace Drupal\Tests\media_twig_tools\Unit\Base;

// phpcs:disable SlevomatCodingStandard.Namespaces.AlphabeticallySortedUses.IncorrectlyOrderedUses
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\FileStorage;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageEffectInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\image\ImageStyleStorage;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\Entity\Media;
use Drupal\media\MediaStorage;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfig;
use Drupal\media_twig_tools\Config\MediaTwigToolsConfigInterface;
use Drupal\media_twig_tools\Entity\MediaTwigToolsEffect;
use Drupal\media_twig_tools\Plugin\MediaTwigToolsFormatter\ImageFormatter;
use Drupal\media_twig_tools\Plugin\MediaTwigToolsFormatter\SimplePictureFormatter;
use Drupal\media_twig_tools\Plugin\MediaTwigToolsFormatter\TypesFallbackPictureFormatter;
use Drupal\media_twig_tools\Services\EffectsManager;
use Drupal\media_twig_tools\Services\EffectsManagerInterface;
use Drupal\media_twig_tools\Services\GeneratedFilesManager;
use Drupal\media_twig_tools\Services\ImageBuilder;
use Drupal\media_twig_tools\Services\ImageBuilderInterface;
use Drupal\media_twig_tools\Services\MediaTwigTools;
use Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManager;
use Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface;
use Drupal\media_twig_tools\Services\MediaTwigToolsInterface;
use Drupal\media_twig_tools\Services\OversizeChecker;
use Drupal\media_twig_tools\Services\OversizeCheckerInterface;
use Drupal\Tests\media_twig_tools\Unit\Traits\ContainerMockTraits;
use Drupal\Tests\media_twig_tools\Unit\Traits\FieldMockTraits;
use Drupal\Tests\media_twig_tools\Unit\Traits\StorageMockTraits;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use http\Client\Response;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Abstract tools for unit tests.
 */
abstract class AbstractFormatterUnitTestCase extends UnitTestCase {

  use ContainerMockTraits;
  use StorageMockTraits;
  use FieldMockTraits;

  /**
   * Test domain.
   *
   * @const string
   */
  public const TEST_DOMAIN = 'domain.com';

  /**
   * Formatters.
   *
   * @var \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface[]
   */
  protected array $formatterPlugins = [];

  /**
   * The formatterManager mock.
   *
   * @var \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface
   */
  protected MediaTwigToolsFormatterPluginManagerInterface $formatterManager;

  /**
   * Entity type manager mock.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected EntityTypeManager|MockObject $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager|\PHPUnit\Framework\MockObject\MockObject
   */
  protected LanguageManager|MockObject $languageManager;

  /**
   * The image builder.
   *
   * @var \Drupal\media_twig_tools\Services\ImageBuilder
   */
  protected ImageBuilder $imageBuilder;

  /**
   * Config.
   *
   * @var array
   */
  protected array $config = [];

  /**
   * Media Twig Tools config.
   *
   * @var \Drupal\media_twig_tools\Config\MediaTwigToolsConfig
   */
  protected MediaTwigToolsConfig $mediaTwigToolsConfig;

  /**
   * The effects manager.
   *
   * @var \Drupal\media_twig_tools\Services\EffectsManager
   */
  protected EffectsManager $effectsManager;

  /**
   * The generated files manager.
   *
   * @var \Drupal\media_twig_tools\Services\GeneratedFilesManager
   */
  protected GeneratedFilesManager $generatedFilesManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem|\PHPUnit\Framework\MockObject\MockObject
   */
  protected FileSystem|MockObject $fileSystem;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MessengerInterface|MockObject $messengerMock;

  /**
   * The oversize checker.
   *
   * @var \Drupal\media_twig_tools\Services\OversizeChecker
   */
  protected OversizeChecker $oversizeChecker;

  /**
   * The media mock.
   *
   * @var \Drupal\media\Entity\Media|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|Media $media;

  /**
   * The request stack mock.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|RequestStack $requestStack;

  /**
   * The HTTP Client mock.
   *
   * @var \GuzzleHttp\Client|\PHPUnit\Framework\MockObject\MockObject
   */
  protected Client|MockObject $httpClient;

  /**
   * The file URL Generator mock.
   *
   * @var \Drupal\Core\File\FileUrlGenerator|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|FileUrlGenerator $fileUrlGenerator;

  /**
   * The media twig tools mock.
   *
   * @var \Drupal\media_twig_tools\Services\MediaTwigTools
   */
  protected MediaTwigTools $mediaTwigTools;

  /**
   * Image style data.
   *
   * @var \WeakMap
   */
  protected \WeakMap $imageStyleData;

  /**
   * Return the tested file uri.
   *
   * @return string
   *   The tested file uri.
   */
  abstract public function getTestFileUri(): string;

  /**
   * Return the pathinfo.
   *
   * @return string
   *   The path info.
   */
  abstract protected function getPathInfo(): string;

  /**
   * Return the file size.
   *
   * @return array
   *   The file size.
   */
  abstract protected function getFileSize(): array;

  /**
   * Return the ist of service mock up build methods.
   *
   * @return string[]
   *   The map id => mockMethod.
   */
  protected function getAvailableServicesMap() {
    return [
      'plugin.manager.media_twig_tools_formatter' => 'getFormatterManagerMock',
      'entity_type.manager' => 'getEntityTypeManagerMock',
      'language_manager' => 'getLanguageManagerMock',
      'messenger' => 'getMessengerMock',
      'media_twig_tools.media_twig_tools' => 'getMediaTwigTools',
      'media_twig_tools.config' => 'getMediaTwigToolsConfig',
      'media_twig_tools.image_builder' => 'getImageBuilder',
      'file_system' => 'getFileSystemMock',
      'media_twig_tools.oversize_checker' => 'getOversizeChecker',
      'media_twig_tools.generated_files_manager' => 'getGeneratedFilesManager',
      'file_url_generator' => 'getFileUrlGeneratorMock',
      'http_client' => 'getHttpClientMock',
      'media_twig_tools.effects_manager' => 'getEffectsManager',
      'request_stack' => 'getRequestStackMock',
      'config.factory' => 'getConfigFactory',
    ];
  }

  /**
   * Return a formatter plugin.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param string $class
   *   The plugin class.
   *
   * @return \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface
   *   The media twig tools formatter plugin manager.
   */
  protected function getFormatterPlugin(string $plugin_id, string $class) {
    if (!isset($this->formatterPlugins[$plugin_id])) {
      if (method_exists($class, 'create')) {
        $plugin = $this->createPartialMock($class, ['t']);

        $container = $this->getContainerMock();
        $methods = [
          'setFileSystem' => 'file_system',
          'setLanguageManager' => 'language_manager',
          'setEntityTypeManager' => 'entity_type.manager',
          'setMessenger' => 'messenger',
          'setFormatterManager' => MediaTwigToolsFormatterPluginManagerInterface::SERVICE_ID,
          'setMediaTwigTools' => MediaTwigToolsInterface::SERVICE_ID,
          'setOversizeChecker' => OversizeCheckerInterface::SERVICE_ID,
          'setImageBuilder' => ImageBuilderInterface::SERVICE_ID,
          'setConf' => MediaTwigToolsConfigInterface::SERVICE_ID,
          'setEffectsManager' => EffectsManagerInterface::SERVICE_ID,
        ];

        foreach ($methods as $method_name => $service_id) {
          $method = $this->getCallableProtectedMethod($plugin, $method_name);
          $method->invokeArgs($plugin, [$container->get($service_id)]);
        }

        $this->addMockCallable($plugin, 't', function ($message, array $data = []) {
          return str_replace(array_keys($data), $data, $message);
        });
        $this->formatterPlugins[$plugin_id] = $plugin;
      }
    }

    return $this->formatterPlugins[$plugin_id];
  }

  /**
   * Return the formatter manager.
   *
   * @return \Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManager|\Drupal\media_twig_tools\Services\MediaTwigToolsFormatterPluginManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   *   The formatter manager.
   */
  protected function getFormatterManagerMock(): MediaTwigToolsFormatterPluginManager|MockObject|MediaTwigToolsFormatterPluginManagerInterface {
    if (!isset($this->formatterManager)) {
      $this->formatterManager = $this->createMock(MediaTwigToolsFormatterPluginManager::class);

      $available_formatter = [
        'image' => ImageFormatter::class,
        'simple_picture' => SimplePictureFormatter::class,
        'types_fallback_picture' => TypesFallbackPictureFormatter::class,
      ];

      // Add getFormatter callback.
      $this->addMockCallable($this->formatterManager, 'getFormatter',
        function ($id) use ($available_formatter) {
          return $this->getFormatterPlugin($id, $available_formatter[$id]);
        }
      );
    }
    return $this->formatterManager;
  }

  /**
   * Return the media twig tools service.
   *
   * @return \Drupal\media_twig_tools\Services\MediaTwigTools
   *   The media twig tools service.
   */
  protected function getMediaTwigTools(): MediaTwigTools {
    if (!isset($this->mediaTwigTools)) {
      $container = $this->getContainerMock();

      $this->mediaTwigTools = new MediaTwigTools(
        $container->get('entity_type.manager'),
        $container->get('language_manager'),
        $container->get('media_twig_tools.image_builder'),
        $container->get('media_twig_tools.config'),
        $container->get('file_system'),
        $container->get('media_twig_tools.oversize_checker'),
        $container->get('plugin.manager.media_twig_tools_formatter'),
        $container->get('media_twig_tools.generated_files_manager')
      );
    }
    return $this->mediaTwigTools;
  }

  /**
   * Return the media twig tools config.
   *
   * @return \Drupal\media_twig_tools\Config\MediaTwigToolsConfig
   *   The media twig tools config.
   */
  protected function getMediaTwigToolsConfig(): MediaTwigToolsConfig {
    if (!isset($this->mediaTwigToolsConfig)) {

      // Build immutable config.
      $immutable_config = $this->createMock(ImmutableConfig::class);
      $this->addMockCallable($immutable_config, 'get',
        function ($id) {
          return $this->config[$id] ?? NULL;
        }
      );

      // Build editable config.
      $config = $this->createMock(Config::class);
      $this->addMockCallable($config, 'get',
        function ($id) {
          return $this->config[$id] ?? NULL;
        }
      );
      $this->addMockCallable($config, 'set',
        function ($id, $value) use ($config) {
          $this->config[$id] = $value;
          return $config;
        }
      );

      // Build config factory.
      $config_factory = $this->createMock(ConfigFactory::class);
      $this->addMockCallable($config_factory, 'get',
        function ($id) use ($immutable_config) {
          return $immutable_config;
        });
      $this->addMockCallable($config_factory, 'getEditable',
        function ($id) use ($config) {
          return $config;
        });

      $this->mediaTwigToolsConfig = new MediaTwigToolsConfig($config_factory);
      $this->mediaTwigToolsConfig->setDirname('builder');
      $this->mediaTwigToolsConfig->setNoStyle('no_style');
      $this->mediaTwigToolsConfig->setAllowMemoryLimit(1024);
      $this->mediaTwigToolsConfig->enableFormatCheck(TRUE);
      $this->mediaTwigToolsConfig->enableOversizeCheck(TRUE);
      $this->mediaTwigToolsConfig->enableOldEffectsFormat(FALSE);
    }

    return $this->mediaTwigToolsConfig;
  }

  /**
   * Return the image builder.
   *
   * @return \Drupal\media_twig_tools\Services\ImageBuilder
   *   The image builder.
   */
  protected function getImageBuilder(): ImageBuilder {
    if (!isset($this->imageBuilder)) {
      $container = $this->getContainerMock();

      $this->imageBuilder = new ImageBuilder(
        $container->get('media_twig_tools.config'),
        $container->get('file_url_generator'),
        $container->get('http_client'),
        $container->get('media_twig_tools.effects_manager'),
        $container->get('media_twig_tools.generated_files_manager'),
        $container->get('media_twig_tools.oversize_checker'),
        $container->get('request_stack'),
        $container->get('entity_type.manager'),
      );
    }
    return $this->imageBuilder;
  }

  /**
   * Return the effects manager.
   *
   * @return \Drupal\media_twig_tools\Services\EffectsManager
   *   The effects manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEffectsManager(): EffectsManager {
    if (!isset($this->effectsManager)) {
      $this->effectsManager = $this->createPartialMock(EffectsManager::class, []);
      $this->setProtectedProperty($this->effectsManager, 'storage', $this->createStorageMock('media_twig_tools_effect', [
        'method' => 'getMediaTwigToolsEffect',
        'class' => ContentEntityStorageBase::class,
      ]));
    }
    return $this->effectsManager;
  }

  /**
   * Return the generated files manager.
   *
   * @return \Drupal\media_twig_tools\Services\GeneratedFilesManager
   *   The generated files manager.
   */
  protected function getGeneratedFilesManager() {
    if (!isset($this->generatedFilesManager)) {
      $container = $this->getContainerMock();

      $this->generatedFilesManager = new GeneratedFilesManager(
        $container->get('file_system'),
        $container->get('media_twig_tools.config'),
      );
    }
    return $this->generatedFilesManager;
  }

  /**
   * Return the messenger.
   *
   * @return \Drupal\Core\Messenger\Messenger|\PHPUnit\Framework\MockObject\MockObject
   *   The messenger.
   */
  protected function getMessengerMock() {
    if (!isset($this->messengerMock)) {
      $this->messengerMock = $this->createPartialMock(Messenger::class, ['addError']);

      $this->addMockCallable($this->messengerMock, 'addError',
        function ($value) {
          throw new \Exception($value);
        });
    }
    return $this->messengerMock;
  }

  /**
   * Return the oversize checker.
   *
   * @return \Drupal\media_twig_tools\Services\OversizeChecker
   *   The oversize checker.
   */
  protected function getOversizeChecker() {
    if (!isset($this->oversizeChecker)) {
      $this->oversizeChecker = $this->createPartialMock(
        OversizeChecker::class,
        ['getOriginalFileDimension', 'getImageFileDimensions']
      );
      $this->setProtectedProperty($this->oversizeChecker, 'entityTypeManager', $this->getEntityTypeManagerMock());
      $this->setProtectedProperty($this->oversizeChecker, 'conf', $this->getMediaTwigToolsConfig());

      $this->addMockCallable($this->oversizeChecker, 'getOriginalFileDimension',
        function (FileInterface $file) {
          return $this->getFileSize();
        });

      $this->addMockCallable($this->oversizeChecker, 'getImageFileDimensions',
        function (string $file) {
          return $this->getFileSize();
        });
    }
    return $this->oversizeChecker;
  }

  /**
   * Return the entity type manager mock.
   *
   * @return \Drupal\Core\Entity\EntityTypeManager|\PHPUnit\Framework\MockObject\MockObject
   *   The entity type manager mock.
   */
  protected function getEntityTypeManagerMock(): EntityTypeManager|MockObject {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = $this->createMock(EntityTypeManager::class);

      $available_storages = [
        'media' => [
          'method' => 'getMedia',
          'class' => MediaStorage::class,
        ],
        'media_twig_tools_effect' => [
          'method' => 'getMediaTwigToolsEffect',
          'class' => ContentEntityStorageBase::class,
        ],
        'image_style' => [
          'method' => 'getImageStyle',
          'class' => ImageStyleStorage::class,
        ],
        'file' => [
          'method' => 'getFile',
          'class' => FileStorage::class,
        ],
      ];

      // Add media load callback.
      $this->addMockCallable($this->entityTypeManager, 'getStorage',
        function (string $entity_type) use ($available_storages) {
          return $this->createStorageMock($entity_type, $available_storages[$entity_type] ?? NULL);
        });
    }
    return $this->entityTypeManager;
  }

  /**
   * Return the language manager.
   *
   * @return \Drupal\Core\Language\LanguageManager|\PHPUnit\Framework\MockObject\MockObject
   *   The language manager mock.
   */
  protected function getLanguageManagerMock(): MockObject|LanguageManager {
    if (!isset($this->languageManager)) {
      $this->languageManager = $this->createMock(LanguageManager::class);
      $this->addMockCallable($this->languageManager, 'getCurrentLanguage', function () {
        $language = $this->createMock(Language::class);
        $this->addMockCallable($language, 'getId', function () {
          return 'en';
        });
        return $language;
      });
    }

    return $this->languageManager;
  }

  /**
   * Return the file url generator mock.
   *
   * @return \Drupal\Core\File\FileUrlGenerator|\PHPUnit\Framework\MockObject\MockObject
   *   The file url generator mock.
   */
  protected function getFileUrlGeneratorMock() {
    if (!isset($this->fileUrlGenerator)) {
      $this->fileUrlGenerator = $this->createMock(FileUrlGenerator::class);

      // Init generateAbsoluteString method.
      $this->addMockCallable($this->fileUrlGenerator, 'generateAbsoluteString',
        function ($uri) {
          $path_data = parse_url($uri);
          return isset($path_data['path']) ? 'https://' . self::TEST_DOMAIN . '/sites/default/files/styles' . $path_data['path'] : NULL;
        });
      // Init transformRelative method.
      $this->addMockCallable($this->fileUrlGenerator, 'transformRelative',
        function ($uri) {
          $path_data = parse_url($uri);
          return $path_data['path'] ?? NULL;
        });
      // Init generateString method.
      $this->addMockCallable($this->fileUrlGenerator, 'generateString',
        function ($uri) {
          $path_data = parse_url($uri);
          $prefix = $path_data['scheme'] === 'public' ? '/sites/default/files/styles' : '/sites/default/privates/styles';
          return $prefix . $path_data['path'] ?? NULL;
        });
    }

    return $this->fileUrlGenerator;
  }

  /**
   * Return the http client mock.
   *
   * @return \GuzzleHttp\Client|\PHPUnit\Framework\MockObject\MockObject
   *   The http client.
   */
  public function getHttpClientMock() {
    if (!isset($this->httpClient)) {
      $this->httpClient = $this->createMock(Client::class);
      $this->addMockCallable($this->httpClient, 'get',
        function ($value) {
          return new Response('OK');
        });
    }

    return $this->httpClient;
  }

  /**
   * Return the file system mock.
   *
   * @return \Drupal\Core\File\FileSystem|\PHPUnit\Framework\MockObject\MockObject
   *   The file system mock.
   */
  protected function getFileSystemMock() {
    if (!isset($this->fileSystem)) {
      $this->fileSystem = $this->createMock(FileSystem::class);
    }

    return $this->fileSystem;
  }

  /**
   * Return the request stack mock.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject|\Symfony\Component\HttpFoundation\RequestStack
   *   The request stack mock.
   */
  protected function getRequestStackMock() {
    if (!isset($this->requestStack)) {
      $this->requestStack = $this->createMock(RequestStack::class);

      $this->addMockCallable($this->requestStack, 'getCurrentRequest',
        function () {
          $request = $this->createMock(Request::class);
          $this->addMockCallable($request, 'getPathInfo',
            function () {
              return $this->getPathInfo();
            });

          return $request;
        });
    }

    return $this->requestStack;
  }

  /**
   * Return the tested media.
   *
   * @param array $properties
   *   The properties.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\media\Entity\Media
   *   The media.
   */
  protected function getMedia(array $properties) {
    if (!isset($this->media)) {
      $this->media = $this->createMock(Media::class);

      // ID.
      $this->addMockCallable($this->media, 'id',
        function () use ($properties) {
          return $properties['id'];
        });

      $this->addMockCallable($this->media, 'get',
        function (string $field) use ($properties) {
          switch ($field) {
            case 'field_media_image':
              $file_properties = [
                'id' => $properties['id'] + 10,
              ];

              $entity_type_manager = $this->getEntityTypeManagerMock();
              $file_storage = $entity_type_manager->getStorage('file');
              $file = $file_storage->create($file_properties);

              $field = $this->getFileFieldItemList($file);
              break;

            default:
              $field = NULL;
          }

          return $field;
        });
    }
    return $this->media;
  }

  /**
   * Return the tested file.
   *
   * @param array $properties
   *   The properties.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\file\Entity\File
   *   The file mock.
   */
  public function getFile(array $properties) {
    $file = $this->createMock(File::class);

    $uri = $this->getTestFileUri();
    $this->addProperty($file, 'id', $properties['id']);
    $this->addProperty($file, 'uri', $uri);
    $this->addMockCallable($file, 'id',
      function () use ($properties) {
        return $properties['id'];
      });
    $this->addMockCallable($file, 'getFileUri',
      function () use ($uri) {
        return $uri;
      });
    $this->addMockCallable($file, 'getMimeType',
      function () use ($uri) {
        return 'image/' . pathinfo($uri)['extension'];
      });

    return $file;
  }

  /**
   * Return the media twig tools effect.
   *
   * @param array $properties
   *   The properties.
   *
   * @return \Drupal\media_twig_tools\Entity\MediaTwigToolsEffect
   *   The media twig tools effect.
   */
  protected function getMediaTwigToolsEffect(array $properties): MediaTwigToolsEffect {
    $effect = $this->createMock(MediaTwigToolsEffect::class);
    if (isset($properties['id'])) {
      $this->addProperty($effect, 'token', $properties['id']);
    }
    if (isset($properties['token'])) {
      $this->addProperty($effect, 'id', $properties['token']);
    }

    foreach ($properties as $property_name => $property_value) {
      $this->addProperty($effect, $property_name, $property_value);
    }

    $this->addMockCallable($effect, 'id',
      function () use ($properties) {
        return $properties['token'];
      });

    return $effect;
  }

  /**
   * Return an image style.
   *
   * @param array $properties
   *   The data.
   *
   * @return \Drupal\image\Entity\ImageStyle|\PHPUnit\Framework\MockObject\MockObject
   *   The image style.
   */
  protected function getImageStyle(array $properties) {
    $image_style = $this->createMock(ImageStyle::class);

    // Init converter properties.
    if (in_array($properties['id'], ['jpg', 'jpeg', 'webp'])) {
      $properties['effects'] = [
        [
          'id' => 'image_converter',
          'data' => ['extension' => $properties['id']],
        ],
      ];
    }

    // Save properties.
    if (!isset($this->imageStyleData)) {
      $this->imageStyleData = new \WeakMap();
    }
    $this->imageStyleData[$image_style] = $properties;

    foreach ($properties as $property_name => $property_value) {
      $this->addProperty($image_style, $property_name, $property_value);
    }

    $this->addMockCallable($image_style, 'buildUri',
      function ($uri) use ($image_style) {
        $properties = $this->imageStyleData[$image_style];
        $path_data = explode('://', $uri);
        $extension = $this->getImageStyleExtension($image_style);

        if ($extension !== 'png') {
          $path_data[1] .= '.' . $extension;
        }
        return $path_data[0] . '://styles/' . $properties['id'] . '/' . $path_data[0] . '/' . $path_data[1];
      });

    $this->addMockCallable($image_style, 'id',
      function () use ($properties) {
        return $properties['id'];
      });

    $this->addMockCallable($image_style, 'getDerivativeExtension', function () use ($image_style) {
      return $this->getImageStyleExtension($image_style);
    });

    $this->addMockCallable($image_style, 'getEffects', function () use ($image_style) {
      $effects = $this->createMock(ImageEffectInterface::class);
      $this->addMockCallable($effects, 'getConfiguration', function () use ($image_style) {
        return $this->imageStyleData[$image_style]['effects'] ?? [];
      });
      return $effects;
    });

    $this->addMockCallable($image_style, 'addImageEffect', function ($effect) use ($image_style) {
      $this->imageStyleData[$image_style]['effects'] = $this->imageStyleData[$image_style]['effects'] ?? [];
      $this->imageStyleData[$image_style]['effects'][] = $effect;
    });

    return $image_style;
  }

  /**
   * Return the image style extension.
   *
   * @param \Drupal\image\ImageStyleInterface $image_style
   *   The image style.
   *
   * @return string
   *   The extension.
   */
  protected function getImageStyleExtension(ImageStyleInterface $image_style): string {
    $extension = 'png';
    foreach ($this->imageStyleData[$image_style]['effects'] ?? [] as $effect) {
      if (isset($effect['data']['extension'])) {
        $extension = $effect['data']['extension'];
      }
    }
    return $extension;
  }

  /**
   * Return an image item.
   *
   * @param \Drupal\file\FileInterface|\PHPUnit\Framework\MockObject\MockObject $file
   *   The file.
   *
   * @return \Drupal\image\Plugin\Field\FieldType\ImageItem|\PHPUnit\Framework\MockObject\MockObject
   *   The image item.
   */
  protected function getImageItem(FileInterface|MockObject $file) {
    $image_item_field = $this->createMock(ImageItem::class);
    $size = $this->getFileSize();

    $this->addProperty($image_item_field, 'target_id', $file->id());
    $this->addProperty($image_item_field, 'width', $size['width']);
    $this->addProperty($image_item_field, 'height', $size['height']);
    $this->addMockCallable($image_item_field, 'view',
      function () use ($image_item_field) {
        return [
          "#theme" => "image_formatter",
          "#item" => $image_item_field,
          "#item_attributes" => [
            "loading" => "lazy",
          ],
          "#image_style" => "",
          "#url" => NULL,
          "#cache" => [
            "tags" => [],
            "contexts" => [],
            "max-age" => -1,
          ],
        ];
      });

    return $image_item_field;
  }

  /**
   * Return the file field item list.
   *
   * @param \Drupal\file\FileInterface|MockObject $file
   *   The file.
   *
   * @return \Drupal\file\Plugin\Field\FieldType\FileFieldItemList|\PHPUnit\Framework\MockObject\MockObject
   *   The file field item list.
   */
  protected function getFileFieldItemList(FileInterface|MockObject $file) {
    $field = $this->createMock(FileFieldItemList::class);
    $image_item = $this->getImageItem($file);
    $this->addMockCallable($field, 'first',
      function () use ($image_item) {
        return $image_item;
      });

    $this->addMockCallable($field, 'referencedEntities',
      function () use ($file) {
        return [$file];
      });

    $this->addProperty($field, 'target_id', $file->id());

    return $field;
  }

}
