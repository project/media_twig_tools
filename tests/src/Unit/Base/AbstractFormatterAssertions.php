<?php

namespace Drupal\Tests\media_twig_tools\Unit\Base;

/**
 * Several assertions methods for Media Twig Tools formatter.
 */
abstract class AbstractFormatterAssertions extends AbstractFormatterUnitTestCase {

  /**
   * Return the default null effect token.
   *
   * @return string
   *   The token.
   */
  public function getDefaultNullToken(): string {
    return 'd751713988987e9331980363e24189ce';
  }

  /**
   * {@inheritdoc}
   */
  public function getTestFileUri(): string {
    return 'public://image.png';
  }

  /**
   * {@inheritdoc}
   */
  protected function getPathInfo(): string {
    return '/sites/default/files/styles/public/image.png';
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileSize(): array {
    return [
      'width' => 300,
      'height' => 300,
    ];
  }

  /**
   * Return the expected url according to parameters.
   *
   * @param string|null $style
   *   The Style.
   * @param string|null $token
   *   The Token.
   * @param bool $absolute
   *   Absolute.
   * @param string|null $extension
   *   The extension.
   *
   * @return string
   *   The expected url.
   */
  protected function getExpectedUrl(?string $style = NULL, ?string $token = NULL, bool $absolute = FALSE, ?string $extension = 'png'): string {
    $token = $token ?? $this->getDefaultNullToken();
    $style = $style ?? 'no_style';
    $domain = $absolute ? 'https://' . static::TEST_DOMAIN : '';
    return $domain . '/sites/default/files/styles/builder/' . $style . '/' . $token . '/11_image.' . $extension . $this->getImageBuilder()->getCacheId();
  }

  /**
   * Assert true if the build has the main image src as expected.
   *
   * @param string $expected
   *   The expected src.
   * @param array $build
   *   The build array.
   */
  public function assertMainSrc(string $expected, array $build): void {
    $this->assertArrayHasKey('#item_attributes', $build);
    $this->assertArrayHasKey('data-override-src', $build['#item_attributes']);

    $this->assertEquals($expected, $build['#item_attributes']['data-override-src'], '=> Main SRC not equals');
  }

  /**
   * Assert true if the srcset build contains the expected srcset rule.
   *
   * @param string $media_query
   *   The media query.
   * @param string $url
   *   The URL.
   * @param array $build
   *   The build array.
   */
  public function assertSrcsetContains(string $media_query, string $url, array $build): void {
    $this->assertArrayHasKey('srcset', $build, '=> SRCSET attribute not found');
    $this->assertStringContainsString(trim($url . ' ' . $media_query), $build['srcset'], '=> SRCSET value not found');
  }

  /**
   * Assert true if the build have no srcset.
   *
   * @param array $build
   *   The build array.
   */
  public function assertNotHaveSrcset(array $build): void {
    $this->assertArrayNotHasKey('srcset', $build, '=> SRCSET found, but should not be present');
  }

  /**
   * Assert has attributes with expected value.
   *
   * @param string $name
   *   The attribute name.
   * @param string|null $value
   *   The attribute value.
   * @param array $build
   *   The build array.
   */
  public function assertHasAttributeValue(string $name, ?string $value = NULL, array $build = []): void {
    $this->assertArrayHasKey($name, $build['#item_attributes'], '=> Attribute "' . $name . '" not found');
    $this->assertEquals($build['#item_attributes'][$name], $value, '=> Attribute "' . $name . '" value not correct');
  }

  /**
   * Assert has no attribute.
   *
   * @param string $name
   *   The attribute name.
   * @param array $build
   *   The build array.
   */
  public function assertNotHasAttribute(string $name, array $build = []): void {
    $this->assertArrayNotHasKey($name, $build['#item_attributes'], '=> Attribute "' . $name . '" found, but should not be present');
  }

  /**
   * Assert has source with expected src and media query.
   *
   * @param string $srcset
   *   The expected src.
   * @param array $build
   *   The build array.
   * @param string|null $media_query
   *   The expected media query value.
   * @param string|null $type
   *   The expected type value.
   */
  public function assertHasSourceSrcset(string $srcset, array $build = [], ?string $media_query = NULL, ?string $type = NULL): void {
    $source = $this->getSource([$srcset], $media_query, $build);
    $this->assertNotNull($source, 'Expected Srcset not found.');

    if ($media_query) {
      $this->assertArrayHasKey('media', $source['#attributes'], 'Source not has media query');
      $this->assertEquals($media_query, $source['#attributes']['media'], 'Source media query is not as expected');
    }
    else {
      $this->assertArrayNotHasKey('media', $source['#attributes'], 'Source has media query');
    }

    if ($type) {
      $this->assertArrayHasKey('type', $source['#attributes'], 'Source not has type query');
      $this->assertEquals($type, $source['#attributes']['type'], 'Source type is not as expected');
    }
    else {
      $this->assertArrayNotHasKey('type', $source['#attributes'], 'Source has type');
    }
  }

  /**
   * Assert has source with all expected src in srcset and media query.
   *
   * @param array $srcset
   *   The expected src.
   * @param string|null $media_query
   *   The expected media query value.
   * @param array $build
   *   The build array.
   */
  public function assertHasSourceAllSrcset(array $srcset, ?string $media_query, array $build = []): void {
    $source = $this->getSource($srcset, $media_query, $build);
    $this->assertNotNull($source, 'Expected Srcset not found.');

    $url_data = $this->getSrcsetSeparatedData($source);

    $this->assertEmpty(array_diff_assoc($url_data, $srcset), 'Srcset is not correct');

    if ($media_query) {
      $this->assertArrayHasKey('media', $source['#attributes'], 'Source not has media query');
      $this->assertEquals($media_query, $source['#attributes']['media'], 'Source media query is not as expected');
    }
    else {
      $this->assertArrayNotHasKey('media', $source['#attributes'], 'Source has media query');
    }
  }

  /**
   * Assert has no source.
   *
   * @param array $build
   *   The build array.
   */
  public function assertHasNoSource(array $build = []): void {
    $source = $this->getSource(NULL, NULL, $build);
    $this->assertEmpty($source);
  }

  /**
   * Assert not has source with expected src and media query.
   *
   * @param string $srcset
   *   The expected src.
   * @param string|null $media_query
   *   The expected media query value.
   * @param array $build
   *   The build array.
   */
  public function assertNotHasSourceSrcset(string $srcset, ?string $media_query, array $build = []): void {
    $source = $this->getSource([$srcset], $media_query, $build);
    $this->assertNull($source, 'Expected Src found.');
  }

  /**
   * Return the wanted source.
   *
   * @param array|null $srcset_list
   *   The expected src set list.
   * @param string|null $media_query
   *   The expected media query value.
   * @param array $build
   *   The build array.
   *
   * @return array|null
   *   The source.
   */
  protected function getSource(?array $srcset_list, ?string $media_query, array $build = []): ?array {
    $sources = $build['sources'] ?? [];

    foreach ($sources as $source) {
      $urls_list = $this->getSrcsetSeparatedData($source);

      /*
       * Check if list of srcset in source are strictly the same as
       * the searched one.
       */
      if (
        empty(array_diff($urls_list, $srcset_list))
        && empty(array_diff($srcset_list, $urls_list))
      ) {
        if ($media_query) {
          if ($media_query === $source['#attributes']['media']) {
            return $source;
          }
          return NULL;
        }
        return $source;
      }
    }

    return NULL;
  }

  /**
   * Get srcset urls data in source.
   *
   * @param array $source
   *   The source.
   *
   * @return array
   *   The urls data.
   */
  protected function getSrcsetSeparatedData(array $source = []) {
    if (isset($source['#attributes']['srcset'])) {
      $urls_list = explode(',', $source['#attributes']['srcset']);
      $urls_data = [];
      foreach (array_map(fn($x) => explode(' ', $x), $urls_list) as $url_src) {
        $urls_data[$url_src[1] ?? ''] = $url_src[0];
      }
      return $urls_data;
    }
    return [];
  }

}
