CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

The Media Twig Tools modules brings some features to facilitate the integration
of media entity in twig template, especially for images and pictures. By
default, media are rendered with the drupal rendering process, bringing
potential unnecessary tags to display simple image and picture tags. Media Twig
Tools clean the default rendered markup of images.

It also brings some tools to decouple the theme sources and the configuration
for images. By the default, Drupal brings the image style configuration to deal
with the adaptive / responsive image. This can be very difficult to a front end
developer to integrate these practices when he has to deal with a lot of
several configurations for several image sizes and rendering. Media
Twig Tools provides tools to integrate srcSet settings, and picture settings
(with media queries) directly in twig without needing the definition of specific
configuration for each breakpoint, each image style etc... Media Twig Tools
allow front-end developers to generate styled images at runtime.


REQUIREMENTS
------------

This module requires drupal/media modules.


INSTALLATION
------------

Install the Media Twig Tools module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. G to Administration > Configuration > Media > Media Twig Tools.

Configurable parameters:

* Settings / Generated image directory name : The directory where custom images are
  generated.
* Settings / No image style identifier : The default identifier to indicate
  Media Twig Tools to not use any image style. Make sure it does not match an
  existing image style name.
* Allow memory limit (for image generation) : The memory limit for image generation
* Enable format check : if enabled, only printed urls will be generated. 
* Enable oversize check : if enabled, will not generate bigger image than the source.
* Enable old effects format : For back compatibility only.

MAINTAINERS
-----------

* Thomas Sécher : https://www.drupal.org/u/tsecher
